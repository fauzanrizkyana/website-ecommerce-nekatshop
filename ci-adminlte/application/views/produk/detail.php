<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>produk"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card card-solid">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="col-12">
                            <img src="<?= base_url(); ?>assets/img2/<?= $produk->gambar_produk; ?>" class="product-image" alt="Product Image">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <h3 class="my-3"><?= $produk->nama_produk; ?></h3>
                        <p><?= $produk->deskripsi_produk; ?></p>
                        <p>Jenis Produk : <?= $produk->jenis_produk; ?></p>
                        <p>Jumlah stok : <?= $produk->stok_produk; ?> item</p>
                        <?php
                        $sale = $produk->harga_produk - ($produk->harga_produk * $produk->diskon / 100); ?>
                        <p>Harga awal : Rp. <?= number_format($produk->harga_produk, '0', ',', '.'); ?></p>
                        <p>Diskon : <?= $produk->diskon; ?>%</p>
                        <div class="bg-gray py-2 px-3 mt-4">
                            <h2 class="mb-0">Rp. <?= number_format($sale, '0', ',', '.'); ?></h2>
                        </div>
                        <div class="row mt-3">
                            <div class="col">
                                <a href="<?= base_url(); ?>produk" class="btn btn-primary">Kembali</a>
                                <div class="float-right">
                                    <a href="<?= base_url(); ?>produk/edit/<?= $produk->id_produk; ?>" class="btn btn-warning">edit</a>
                                    <a href="<?= base_url(); ?>produk/hapus/<?= $produk->id_produk; ?>" class="btn btn-danger hapus-produk">hapus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>