<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>produk"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Produk</h3>
            </div>
            <div class="card-body">
                <a href="<?= base_url(); ?>produk" class="btn btn-secondary"><i class="fas fa-angle-left"></i> Kembali</a>
                <?php echo form_open("produk/tambah", array('enctype' => 'multipart/form-data')); ?>
                <div class="row mt-3">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label for="nama_produk" class="col-sm-2 col-form-label">Nama Produk</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?= set_value('nama_produk'); ?>">
                                <?= form_error('nama_produk', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jenis_produk" class="col-sm-2 col-form-label">Jenis Produk</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="jenis_produk">
                                    <?php foreach ($jenis as $j) : ?>
                                        <option value="<?= $j['id']; ?>"><?= $j['jenis_produk']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi_produk" class="col-sm-2 col-form-label">Deskrisi Produk</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="deskripsi_produk" name="deskripsi_produk"><?= set_value('nama_produk'); ?></textarea>
                                <?= form_error('deskripsi_produk', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="harga_produk" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="text" class="form-control" id="harga_produk" name="harga_produk" value="<?= set_value('harga_produk'); ?>">
                                </div>
                                <?= form_error('harga_produk', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="diskon" class="col-sm-2 col-form-label">Diskon</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="diskon" name="diskon" value="<?= set_value('diskon') ? set_value('diskon') : 0; ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                                <?= form_error('diskon', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="stok_produk" class="col-sm-2 col-form-label">Stok</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="stok_produk" name="stok_produk" value="<?= set_value('stok_produk'); ?>">
                                <?= form_error('stok_produk', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gambar_produk" class="col-sm-2 col-form-label">Gambar</label>
                            <div class="col-sm-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="gambar_produk" name="gambar_produk">
                                    <label class="custom-file-label" for="gambar_produk">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </section>
</div>