<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Produk</h3>
                    <a href="<?= base_url(); ?>produk/tambah" class="btn btn-primary float-right">Tambah Data Produk</a>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">Id</th>
                                    <th>Nama Produk</th>
                                    <th>Jenis</th>
                                    <th>Harga</th>
                                    <th>Diskon</th>
                                    <th>Stok</th>
                                    <th>Gambar</th>
                                    <th style="width: 170px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($produk as $prdk) : ?>
                                    <tr>
                                        <td><?= $prdk['id_produk']; ?></td>
                                        <td><?= $prdk['nama_produk']; ?></td>
                                        <td><?= $prdk['jenis_produk']; ?></td>
                                        <td>Rp. <?= number_format($prdk['harga_produk'], '0', ',', '.'); ?></td>
                                        <td><?= $prdk['diskon']; ?>%</td>
                                        <td><?= $prdk['stok_produk']; ?></td>
                                        <td><img src="<?= base_url(); ?>assets/img2/<?= $prdk['gambar_produk']; ?>" width="100"></td>
                                        <td>
                                            <a href="<?= base_url(); ?>produk/detail/<?= $prdk['id_produk']; ?>" class="badge badge-success">detail</a>
                                            <a href="<?= base_url(); ?>produk/edit/<?= $prdk['id_produk']; ?>" class="badge badge-warning">edit</a>
                                            <a href="<?= base_url(); ?>produk/hapus/<?= $prdk['id_produk']; ?>" class="badge badge-danger hapus-produk">hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>