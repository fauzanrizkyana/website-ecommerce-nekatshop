<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>user"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detail</h3>
                </div>
                <div class="card-body">
                    <div class="card bg-light mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-2">
                                <img src="http://localhost/nekatshop/assets/img2/<?= $member['image']; ?>" class="card-img rounded-circle">
                            </div>
                            <div class="col-md-10">
                                <div class="card-body">
                                    <h5><?= $member['username']; ?></h5>
                                    <p class="card-text">Tanggal Lahir : <?= date('d F Y', $member['tanggal_lahir']); ?></p>
                                    <p class="card-text">Jenis Kelamin : <?= $member['jenis_kelamin']; ?></p>
                                    <p class="card-text">Email : <?= $member['email']; ?></p>
                                    <p class="card-text">No. Telepon : <?= $member['no_telepon']; ?></p>
                                    <p class="card-text">Password : <?= $member['password']; ?></p>
                                    <p class="card-text"><small class="text-muted">Sebagai user sejak <?= date('d F Y H:i', $member['date_created']); ?></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <a href="<?= base_url(); ?>user" class="btn btn-primary">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>