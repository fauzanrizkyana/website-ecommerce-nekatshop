<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
    <section class="content">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" style="width: 100px; height: 100px;" src="<?= base_url(); ?>assets/img5/<?= $user['image']; ?>" alt="User profile picture">
                        </div>
                        <h3 class="profile-username text-center"><?= $user['username']; ?></h3>
                        <ul class="list-group list-group-unbordered mb-3 mt-4">
                            <li class="list-group-item">
                                <b>Email</b> <a class="float-right"><?= $user['email']; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Sebagai admin sejak</b> <a class="float-right"><?= date('d F Y H:i', $user['date_created']); ?></a>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <a href="<?= base_url(); ?>admin/edit" class="btn btn-primary btn-block"><i class="fas fa-user-edit"></i> <b>Edit Profil</b></a>
                                <a href="<?= base_url(); ?>admin/changeemail" class="btn btn-warning btn-block"><i class="fas fa-envelope"></i> <b>Ganti Email</b></a>
                                <a href="<?= base_url(); ?>admin/changepassword" class="btn btn-danger btn-block"><i class="fas fa-key"></i> <b>Ganti Password</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>