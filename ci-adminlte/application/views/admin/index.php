<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tabel Data Admin</h3>
                    <a href="<?= base_url(); ?>admin/tambah" class="btn btn-primary float-right">Tambah anggota</a>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th style="width: 100px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($admin as $a) : ?>
                                    <tr>
                                        <th><?= $i++; ?></th>
                                        <td><?= $a['username']; ?></td>
                                        <td><?= $a['email']; ?></td>
                                        <td>
                                            <a href="<?= base_url(); ?>admin/detail/<?= $a['id_admin']; ?>" class="badge badge-success">detail</a>
                                            <?php if ($a['username'] != $user['username'] && $a['username'] != 'Super Admin') : ?>
                                                <a href="<?= base_url(); ?>admin/hapus/<?= $a['id_admin']; ?>" class="badge badge-danger hapus-produk">hapus</a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>