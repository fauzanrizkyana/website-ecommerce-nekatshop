<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>admin/profil"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?= $subjudul; ?></h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="<?= base_url(); ?>admin/profil" class="btn btn-secondary"><i class="fas fa-angle-left"></i> Kembali</a>
                        <?= $this->session->flashdata('message'); ?>
                        <form action="<?= base_url('admin/changepassword'); ?>" method="post">
                            <div class="form-group">
                                <label for="current_password">Password saat ini</label>
                                <input type="password" class="form-control" id="current_password" name="current_password">
                                <?= form_error('current_password', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="new_password1">Password baru</label>
                                <input type="password" class="form-control" id="new_password1" name="new_password1">
                                <?= form_error('new_password1', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="new_password2">Konfirmasi password</label>
                                <input type="password" class="form-control" id="new_password2" name="new_password2">
                                <?= form_error('new_password2', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Ganti Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>