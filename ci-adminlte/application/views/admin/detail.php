<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>admin"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">

        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detail</h3>
                </div>
                <div class="card-body">
                    <div class="card bg-light mb-3" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="<?= base_url(); ?>assets/img5/<?= $admin->image; ?>" class="card-img rounded-circle">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5><?= $admin->username; ?></h5>
                                    <p class="card-text">Email : <?= $admin->email; ?></p>
                                    <p class="card-text">Password : <?= $admin->password; ?></p>
                                    <p class="card-text"><small class="text-muted">Sebagai admin sejak <?= date('d F Y H:i', $admin->date_created); ?></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <a href="<?= base_url(); ?>admin" class="btn btn-primary">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>