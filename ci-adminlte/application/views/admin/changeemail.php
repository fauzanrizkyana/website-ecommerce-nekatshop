<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>admin/profil"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?= $subjudul; ?></h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="<?= base_url(); ?>admin/profil" class="btn btn-secondary"><i class="fas fa-angle-left"></i> Kembali</a>
                        <?= $this->session->flashdata('message'); ?>
                        <form action="<?= base_url('admin/changeemail'); ?>" method="post">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email" value="<?= $user['email'] ?>">
                                <?= form_error('email', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Ganti email</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>