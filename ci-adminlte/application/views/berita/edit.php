<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>berita"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Ubah Data Berita</h3>
            </div>
            <div class="card-body">
                <a href="<?= base_url(); ?>berita" class="btn btn-secondary"><i class="fas fa-angle-left"></i> Kembali</a>
                <?php echo form_open("", array('enctype' => 'multipart/form-data')); ?>
                <div class="row mt-3">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <input type="hidden" name="id_berita" value="<?= $berita->id_berita; ?>">
                            <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="judul" value="<?= $berita->judul; ?>">
                                <?= form_error('judul', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-2 col-form-label">Penulis</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="penulis" value="<?= $berita->penulis; ?>">
                                <?= form_error('penulis', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="tanggal" value="<?= $berita->tanggal; ?>">
                            <label for="isi_berita" class="col-sm-2 col-form-label">Isi</label>
                            <div class="col-sm-10">
                                <textarea class="textarea" name="isi_berita" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                    <?= $berita->isi_berita; ?>
                                </textarea>
                                <?= form_error('isi_berita', '<small class="form-text text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gambar" class="col-sm-2 col-form-label">Gambar</label>
                            <div class="col-sm-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="gambar" name="gambar">
                                    <label class="custom-file-label" for="gambar">Choose file</label>
                                    <input type="hidden" name="gambar_lama" value="<?= $berita->gambar; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-primary" name="submit" value="Ubah">
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </section>
</div>