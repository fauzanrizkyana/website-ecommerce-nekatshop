<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Blog</h3>
                    <a href="<?= base_url(); ?>berita/tambah" class="btn btn-primary float-right">Buat Berita</a>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Judul</th>
                                    <th>Tanggal</th>
                                    <th>Penulis</th>
                                    <th>Gambar</th>
                                    <th>Isi</th>
                                    <th style="width: 170px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($berita as $brt) : ?>
                                    <tr>
                                        <th><?= $no++; ?></th>
                                        <td><?= $brt['judul']; ?></td>
                                        <td><?= date('d M Y H:i', $brt['tanggal']); ?></td>
                                        <td><?= $brt['penulis']; ?></td>
                                        <td><?= character_limiter($brt['isi_berita'], 55); ?></td>
                                        <td><img src="<?= base_url(); ?>assets/img3/<?= $brt['gambar']; ?>" width="100"></td>
                                        <td>
                                            <a href="<?= base_url(); ?>berita/detail/<?= $brt['id_berita']; ?>" class="badge badge-primary">detail</a>
                                            <a href="<?= base_url(); ?>berita/edit/<?= $brt['id_berita']; ?>" class="badge badge-success">edit</a>
                                            <a href="<?= base_url(); ?>berita/hapus/<?= $brt['id_berita']; ?>" class="badge badge-danger hapus-banner">hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>