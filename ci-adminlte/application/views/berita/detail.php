<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>berita"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <h2><?= $berita->judul; ?></h2>
                        <h5><?= date('d F Y H:i', $berita->tanggal); ?> | Diposting oleh <?= $berita->penulis; ?></h5>
                        <img src="<?= base_url(); ?>assets/img3/<?= $berita->gambar; ?>" width="500">
                        <?= $berita->isi_berita; ?>
                        <div class="row mt-3">
                            <div class="col">
                                <a href="<?= base_url(); ?>berita" class="btn btn-primary">Kembali</a>
                                <div class="float-right">
                                    <a href="<?= base_url(); ?>berita/edit/<?= $berita->id_berita; ?>" class="btn btn-success">edit</a>
                                    <a href="<?= base_url(); ?>berita/hapus/<?= $berita->id_berita; ?>" class="btn btn-danger hapus-banner">hapus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>