<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
    <section class="content">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Kategori</h3>
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#tambahKategori">
                        Tambah Kategori
                    </button>
                </div>
                <div class="card-body">
                    <table class="table table-hover" id="tabelproduk">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Kategori</th>
                                <th style="width: 100px;">Aksi</th>
                                <th style="width: 150px;">Akses untuk user</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($kategori as $k) : ?>
                                <tr>
                                    <th><?= $i++; ?></th>
                                    <td><a href=></a><?= $k['jenis_produk']; ?></td>
                                    <td>
                                        <a href="<?= base_url(); ?>produk?category=<?= $k['id']; ?>" class="badge badge-primary">buka</a>
                                        <?php if ($k['id'] > 3) : ?>
                                            <a href="<?= base_url(); ?>kategori/hapus/<?= $k['id']; ?>" class="badge badge-danger hapus-produk">hapus</a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="form-check-input cek" <?= $k['active'] == 1 ? 'checked="checked"' : '' ?> data-role="<?= $k['id']; ?>" data-menu="<?= $k['active']; ?>">
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="tambahKategori" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?= base_url('kategori'); ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="jenis_produk">Nama Kategori</label>
                                <input type="text" class="form-control" id="jenis_produk" name="jenis_produk">
                            </div>
                            <div class="form-group">
                                <label for="kependekan">Nama kependekan dari kategori</label>
                                <input type="text" class="form-control" id="kependekan" name="kependekan">
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="active" name="active" value="active">
                                <label class="form-check-label" for="active">Active</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>