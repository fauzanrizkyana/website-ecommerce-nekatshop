<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?= $banner; ?></h3>
                            <p>Banner</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-md-images"></i>
                        </div>
                        <a href="<?= base_url(); ?>banner" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?= $admin; ?></h3>
                            <p>Admin</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-md-person"></i>
                        </div>
                        <a href="<?= base_url(); ?>admin" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?= $member; ?></h3>
                            <p>User</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-md-person"></i>
                        </div>
                        <a href="<?= base_url(); ?>user" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?= $kategori; ?></h3>
                            <p>Kategori Produk</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-md-list"></i>
                        </div>
                        <a href="<?= base_url(); ?>kategori" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?= $produk; ?></h3>
                            <p>Produk</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-md-shirt"></i>
                        </div>
                        <a href="<?= base_url(); ?>produk" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>0</h3>
                            <p>Transaksi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-md-card"></i>
                        </div>
                        <a href="<?= base_url(); ?>transaksi" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?= $berita; ?></h3>
                            <p>Berita</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-copy"></i>
                        </div>
                        <a href="<?= base_url(); ?>berita" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?= $inbox; ?></h3>
                            <p>Kotak Masuk</p>
                        </div>
                        <div class="icon">
                            <i class="far fa-envelope"></i>
                        </div>
                        <a href="<?= base_url(); ?>inbox" class="small-box-footer">Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>