<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
        <div class="card">
            <?php foreach ($about as $a) : ?>
                <div class="card-header">
                    <a href="<?= base_url(); ?>about/edit/<?= $a['id_about']; ?>" class="btn btn-primary">Edit About</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>About</h1>
                            <img src="<?= base_url(); ?>assets/img4/<?= $a['gambar_about']; ?>" alt="" width="400px">
                            <?= $a['isi_about']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
</div>