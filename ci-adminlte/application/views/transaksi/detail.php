<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>transaksi"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Order</h3>
            </div>
            <div class="card-body">
                <p>Kode order: <?= $transaksi['kode_order']; ?></p>
                <p>Tanggal pesan: <?= date('d F Y H:i', $transaksi['tanggal_order']); ?></p>
                <p>Username: <?= $transaksi['username']; ?></p>
                <p>Email: <?= $transaksi['email']; ?></p>
                <p>No. Telepon: <?= $transaksi['no_telepon']; ?></p>
                <p>Alamat tujuan: <?= $transaksi['alamat']; ?> <?= $transaksi['nama_kota']; ?> <?= $transaksi['postal_code']; ?> <?= $transaksi['nama_provinsi']; ?></p>
                <p>Rincian barang:</p>
                <p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Id Produk</th>
                                <th>Nama Produk</th>
                                <th>Ukuran</th>
                                <th>Harga Satuan</th>
                                <th>Qty</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            $grandtotal = 0;
                            foreach ($item as $it) :
                                $grandtotal = $grandtotal + $it['harga_total']; ?>
                                <tr>
                                    <th><?= $no++; ?></th>
                                    <td><?= $it['id_produk']; ?></td>
                                    <td><?= $it['nama_produk']; ?></td>
                                    <td><?= $it['ukuran']; ?></td>
                                    <td>Rp. <?= number_format($it['harga_satuan'], '0', ',', '.'); ?></td>
                                    <td><?= $it['qty'] ?></td>
                                    <td>Rp. <?= number_format($it['harga_total'], '0', ',', '.'); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <th colspan="6">Subtotal</th>
                                <td>Rp. <?= number_format($grandtotal, '0', ',', '.'); ?></td>
                            </tr>
                            <tr>
                                <th colspan="6">Biaya Ongkir (<?= $transaksi['kurir'] ?> | <?= $transaksi['jenis_paket'] ?> | <?= $transaksi['lama_pengiriman'] ?> Hari)</th>
                                <td>Rp. <?= number_format($transaksi['harga_pengiriman'], '0', ',', '.'); ?></td>
                            </tr>
                            <?php $total = $grandtotal + $transaksi['harga_pengiriman']; ?>
                            <tr>
                                <th colspan="6">Total</th>
                                <td>Rp. <?= number_format($total, '0', ',', '.'); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </p>
                <h5>Status:
                    <?php if ($transaksi['status_order'] == '0') : ?>
                        <span class="badge badge-danger">Belum</span>
                    <?php else : ?>
                        <span class="badge badge-success">Lunas</span>
                    <?php endif; ?>
                </h5>
                <div class="card-footer">
                    <a href="<?= base_url(); ?>transaksi" class="btn btn-primary"><i class="fas fa-angle-left"></i> Kembali</a>
                </div>
            </div>
    </section>
</div>