<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Transaksi</h3>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Kode order</th>
                                    <th>Tanggal_order</th>
                                    <th>Username</th>
                                    <th>Alamat</th>
                                    <th>Status</th>
                                    <th style="width: 130px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($transaksi as $t) : ?>
                                    <tr>
                                        <th><?= $no++; ?></th>
                                        <td><?php cetak($t['kode_order']); ?></td>
                                        <td><?= date('d M Y H:i', $t['tanggal_order']); ?></td>
                                        <td><?php cetak($t['username']); ?></td>
                                        <td><?php
                                            $alamat = $t['alamat'] . ' ' . $t['nama_kota'] . ' ' . $t['postal_code'] . ' ' . $t['nama_provinsi'];
                                            cetak(character_limiter($alamat, 20, '')); ?>
                                        </td>
                                        <td>
                                            <?php if ($t['status_order'] == '0') : ?>
                                                <span class="badge badge-danger">Belum</span>
                                            <?php else : ?>
                                                <span class="badge badge-success">Lunas</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url(); ?>transaksi/detail/<?= $t['id_order']; ?>" class="badge badge-success">detail</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>