<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nekatshop Admin | <?= $judul; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/AdminLTE/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/ionicons-master/docs/css/ionicons.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/AdminLTE/plugins/toastr/toastr.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/AdminLTE/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/AdminLTE/plugins/summernote/summernote-bs4.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-blue navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?= base_url(); ?>" class="nav-link"><i class="fas fa-home"></i> Home</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#exampleModal">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        Logout
                    </a>
                </li>
            </ul>
        </nav>
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <a href="<?php echo base_url(); ?>" class="brand-link">
                <h1 class="brand-image"><b>N</b></h1>
                <span class="brand-text font-weight-light"><b>NEKATSHOP Admin</b></span>
            </a>
            <div class="sidebar">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="<?php echo base_url(); ?>assets/img5/<?= $user['image']; ?>" class="img-circle elevation-2 rounded-circle" style="width: 35px; height: 35px;" width="30" height="30" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="<?= base_url(); ?>admin/profil" class="d-block"><?= $user['username']; ?></a>
                    </div>
                </div>
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>" class="nav-link <?= $this->uri->segment(1) == '' ? 'active' : $this->uri->segment(1) == 'dashboard' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>admin" class="nav-link <?= $this->uri->segment(1) == 'admin' ? $this->uri->segment(2) == '' ? 'active' : $this->uri->segment(2) == 'detail' ? 'active' : $this->uri->segment(2) == 'tambah' ? 'active' : '' : ''; ?>">
                                <i class="nav-icon fas fa-user-tie"></i>
                                <p>
                                    Admin
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>user" class="nav-link <?= $this->uri->segment(1) == 'user' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    User
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>banner" class="nav-link <?= $this->uri->segment(1) == 'banner' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-images"></i>
                                <p>
                                    Banner
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>kategori" class="nav-link <?= $this->uri->segment(1) == 'kategori' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-list"></i>
                                <p>
                                    Kategori Produk
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link <?= $this->uri->segment(1) == 'produk' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-tshirt"></i>
                                <p>
                                    Produk
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?= base_url(); ?>produk" class="nav-link <?= $this->uri->segment(1) == 'produk' ? $this->input->get('category') == null ? 'active' : '' : ''; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Semua Produk</p>
                                    </a>
                                </li>
                                <?php $jenis = $this->db->get('t_kategori')->result_array();
                                foreach ($jenis as $j) : ?>
                                    <li class="nav-item">
                                        <a href="<?= base_url(); ?>produk?category=<?= $j['id']; ?>" class="nav-link <?= $this->uri->segment(1) == 'produk' ? $this->input->get('category') == $j['id'] ? 'active' : '' : ''; ?>">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p><?= $j['jenis_produk']; ?></p>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>berita" class="nav-link <?= $this->uri->segment(1) == 'berita' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-newspaper"></i>
                                <p>
                                    Berita
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>about" class="nav-link <?= $this->uri->segment(1) == 'about' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-address-card"></i>
                                <p>
                                    About
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url(); ?>transaksi" class="nav-link <?= $this->uri->segment(1) == 'transaksi' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-credit-card"></i>
                                <p>
                                    Transaksi
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link <?= $this->uri->segment(1) == 'inbox' ? 'active' : ''; ?>">
                                <i class="nav-icon fas fa-inbox"></i>
                                <p>
                                    Inbox
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?= base_url(); ?>inbox" class="nav-link <?= $this->uri->segment(1) == 'inbox' ? $this->uri->segment(2) == '' ? 'active' : '' : ''; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Feedback</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= base_url(); ?>inbox/paymentconfirmation" class="nav-link <?= $this->uri->segment(1) == 'inbox' ? $this->uri->segment(2) == 'paymentconfirmation' ? 'active' : '' : ''; ?>">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Konfirmasi Pembayaran</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>