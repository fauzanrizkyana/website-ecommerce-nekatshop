<footer class="main-footer">
    <strong>Copyright &copy;<?= date('Y'); ?> <a href="#">NEKATSHOP Admin</a>.</strong> All rights
    reserved.
</footer>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pemberitahuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah yakin untuk keluar?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-primary keluar">Ya</button>
            </div>
        </div>
    </div>
</div>
<aside class="control-sidebar control-sidebar-dark">
</aside>
</div>
<!-- jQuery -->
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>assets/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>assets/AdminLTE/dist/js/demo.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- Summernote -->
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $(function() {
        $('.textarea').summernote()
    })
</script>
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="<?= base_url(); ?>assets/js/datatable.js"></script>
<!-- Toastr -->
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/toastr/toastr.min.js"></script>
<script src="<?= base_url(); ?>assets/AdminLTE/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url(); ?>assets/js/myscript.js"></script>
<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
</script>
<script src="<?= base_url(); ?>assets/jquery-number/jquery.number.js"></script>
<script>
    $('#harga_produk').number(true, 0, ',', '.');
</script>
<script>
    $('.cek').on('click', function() {
        const active = $(this).data('menu');
        const idKategori = $(this).data('role');
        $.ajax({
            url: "<?= base_url('kategori/changeactive'); ?>",
            type: 'post',
            data: {
                active: active,
                idKategori: idKategori
            },
            success: function() {
                document.location.href = "<?= base_url('kategori'); ?>"
            }
        });
    });
</script>
</body>

</html>