<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Kotak Masuk</h3>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Tanggal</th>
                                    <th>Subjek</th>
                                    <th style="width: 130px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($inbox as $i) : ?>
                                    <tr>
                                        <th><?= $no++; ?></th>
                                        <td><?php cetak($i['nama']); ?></td>
                                        <td><?php cetak($i['email']); ?></td>
                                        <td><?= date('d M Y H:i', $i['tanggal']); ?></td>
                                        <td><?php cetak(character_limiter($i['subjek'], 10, '')); ?></td>
                                        <td>
                                            <a href="<?= base_url(); ?>inbox/detail/<?= $i['id']; ?>" class="badge badge-success">detail</a>
                                            <a href="<?= base_url(); ?>inbox/hapus/<?= $i['id']; ?>" class="badge badge-danger tombol-hapus">hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>