<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>inbox"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card card-outline">
            <div class="card-header">
                <h3 class="card-title">Pesan</h3>
            </div>
            <div class="card-body">
                <div class="mailbox-read-info">
                    <h5><?php cetak($inbox->subjek); ?></h5>
                    <h6>Nama: <?php cetak($inbox->nama); ?></h6>
                    <h6>Email: <?php cetak($inbox->email); ?>
                        <span class="mailbox-read-time float-right"><?= date('d F Y H:i', $inbox->tanggal); ?></span></h6>
                </div>
                <div class="mailbox-read-message">
                    <?php cetak($inbox->pesan); ?>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="<?= base_url(); ?>inbox/hapus/<?= $inbox->id; ?>" class="btn btn-danger tombol-hapus"><i class="far fa-trash-alt"></i> Hapus</a>
                </div>
                <a href="<?= base_url(); ?>inbox" class="btn btn-primary"><i class="fas fa-angle-left"></i> Kembali</a>
            </div>
        </div>
    </section>
</div>