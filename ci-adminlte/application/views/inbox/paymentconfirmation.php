<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="col">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Kotak Masuk</h3>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Kode Order</th>
                                    <th>Tanggal Pembayaran</th>
                                    <th>Nama Nasabah</th>
                                    <th>Jumlah Transfer</th>
                                    <th style="width: 130px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($payment as $p) : ?>
                                    <tr>
                                        <th><?= $no++; ?></th>
                                        <td><?php cetak($p['kode_order']); ?></td>
                                        <td><?= date('d M Y', $p['tanggal_pembayaran']); ?></td>
                                        <td><?php cetak($p['nama_nasabah']); ?></td>
                                        <td>Rp. <?= number_format($p['jumlah_transfer'], '0', ',', '.'); ?></td>
                                        <td>
                                            <a href="<?= base_url(); ?>inbox/paymentdetail/<?= $p['id_pembayaran']; ?>" class="badge badge-success">detail</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>