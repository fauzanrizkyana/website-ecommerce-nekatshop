<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>transaksi"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Order</h3>
            </div>
            <div class="card-body">
                <p>Kode order: <?= $payment['kode_order']; ?></p>
                <p>Tanggal pesan: <?= date('d F Y H:i', $payment['tanggal_pembayaran']); ?></p>
                <p>Username: <?= $payment['nama_nasabah']; ?></p>
                <p>Email: <?= $payment['email']; ?></p>
                <p>Bank: <?= $payment['nama_bank']; ?></p>
                <p>Jumlah transfer: Rp. <?= number_format($payment['jumlah_transfer'], '0', ',', '.'); ?></p>
                <p>Keterangan:
                    <?php if ($payment['keterangan_pembayaran'] != null) : ?>
                        <?= $payment['keterangan_pembayaran'] ?>
                    <?php else : ?>
                        Tidak ada keterangan
                    <?php endif; ?>
                </p>
                <p>Bukti gambar:</p>
                <img src="http://localhost/nekatshop/assets/img3/<?= $payment['gambar_bukti']; ?>" class="img-thumbnail" width="400">
                <div class="card-footer">
                    <a href="<?= base_url(); ?>inbox/paymentconfirmation" class="btn btn-primary"><i class="fas fa-angle-left"></i> Kembali</a>
                </div>
            </div>
    </section>
</div>