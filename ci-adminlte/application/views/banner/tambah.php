<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>banner"><?= $judul; ?></a></li>
                        <li class="breadcrumb-item active"><?= $subjudul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Gambar</h3>
            </div>
            <div class="card-body">
                <a href="<?= base_url(); ?>banner" class="btn btn-secondary"><i class="fas fa-angle-left"></i> Kembali</a>
                <?php echo form_open("banner/tambah", array('enctype' => 'multipart/form-data')); ?>
                <div class="row mt-3">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label for="input_gambar" class="col-sm-2 col-form-label">Gambar</label>
                            <div class="col-sm-10">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="input_gambar" name="input_gambar">
                                    <label class="custom-file-label" for="input_gambar">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </section>
</div>