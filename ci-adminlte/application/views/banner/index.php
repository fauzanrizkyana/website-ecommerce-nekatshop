<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $judul; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
        <h2>Preview Banner</h2><br>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php foreach ($gambar as $key => $gmbr) : ?>
                    <?php $active = ($key == 0) ? 'active' : ''; ?>
                    <div class="carousel-item <?= $active; ?>">
                        <img src="<?= base_url(); ?>assets/img/<?= $gmbr->nama_file; ?>" class="d-block w-100" alt="...">
                    </div>
                <?php endforeach; ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <br>
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Banner</h3>
                    <a href="<?= base_url(); ?>banner/tambah" class="btn btn-primary float-right">Tambah Data</a>
                </div>
                <div class="card-body">
                    <div class="container">
                        <table class="table table-hover" id="tabelproduk">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Gambar</th>
                                    <th>Nama File</th>
                                    <th>Ukuran File</th>
                                    <th>Tipe File</th>
                                    <th style="width: 150px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($gambar as $gmbr) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><img src="<?= base_url(); ?>assets/img/<?= $gmbr->nama_file; ?>" width="100"></td>
                                        <td><?= $gmbr->nama_file; ?></td>
                                        <td><?= $gmbr->ukuran_file; ?></td>
                                        <td><?= $gmbr->tipe_file; ?></td>
                                        <td>
                                            <a href="<?= base_url(); ?>banner/detail/<?= $gmbr->id; ?>" class="badge badge-success">detail</a>
                                            <a href="<?= base_url(); ?>banner/hapus/<?= $gmbr->id; ?>" class="badge badge-danger hapus-banner">hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>