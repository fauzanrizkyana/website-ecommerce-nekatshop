<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Admin";
        $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
        $data['admin'] = $this->admin_model->view();
        $this->load->view('templates/header', $data)->view('admin/index', $data)->view('templates/footer');
    }
    public function tambah()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_admin.email]', [
            'is_unique' => 'Email ini sudah digunakan'
        ]);
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[password_konfirmasi]', [
            'matches' => 'Password tidak cocok',
            'min_length' => 'Password terlalu pendek'
        ]);
        $this->form_validation->set_rules('password_konfirmasi', 'Password', 'required|trim|matches[password]');
        if ($this->form_validation->run() == false) {
            $data['judul'] = "Admin";
            $data['subjudul'] = "Tambah";
            $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
            $this->load->view('templates/header', $data)->view('admin/tambah', $data)->view('templates/footer');
        } else {
            $this->admin_model->tambah();
            $this->session->set_flashdata('flash', 'Anggota telah ditambahkan.');
            redirect('admin');
        }
    }
    public function hapus($id)
    {
        $this->admin_model->delete($id);
        $this->session->set_flashdata('flash', 'Anggota berhasil dihapus');
        redirect('admin');
    }
    public function profil()
    {
        $data['judul'] = "Profil";
        $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
        $this->load->view('templates/header', $data)->view('admin/profil', $data)->view('templates/footer');
    }
    public function detail($id_admin)
    {
        $data['judul'] = "Admin";
        $data['subjudul'] = "Detail";
        $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
        $data['admin'] = $this->admin_model->getById($id_admin);
        $this->load->view('templates/header', $data)->view('admin/detail', $data)->view('templates/footer');
    }
    public function edit()
    {
        $data['judul'] = "Profil";
        $data['subjudul'] = "Edit Profil";
        $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data)->view('admin/edit', $data)->view('templates/footer');
        } else {
            $this->admin_model->ubahProfil($data['user']['image']);
            $data = [
                'nama' => $this->input->post('username'),
                'mail' => $this->input->post('email'),
                'login' => "loggedin"
            ];
            $this->session->set_userdata($data);
            $this->session->set_flashdata('flash', 'Profil berhasil diubah');
            redirect('admin/profil');
        }
    }
    public function changePassword()
    {
        $data['judul'] = "Profil";
        $data['subjudul'] = "Ganti Password";
        $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
        $this->form_validation->set_rules('current_password', 'Password Saat Ini', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'Password Baru', 'required|trim|min_length[3]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Konfirmasi Password Baru', 'required|trim|min_length[3]|matches[new_password1]');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data)->view('admin/changepassword', $data)->view('templates/footer');
        } else {
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password1');
            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password saat ini salah</div>');
                redirect('admin/changepassword');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password baru tidak boleh sama dengan password saat ini</div>');
                    redirect('admin/changepassword');
                } else {
                    $this->admin_model->gantiPassword($new_password, $data['user']['email']);
                    $this->session->set_flashdata('flash', 'Password anda telah diubah');
                    redirect('admin/profil');
                }
            }
        }
    }
    public function changeEmail()
    {
        $data['judul'] = "Profil";
        $data['subjudul'] = "Ganti Email";
        $data['user'] = $this->admin_model->getUser($this->session->userdata('mail'));
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_admin.email]', [
            'is_unique' => 'Email ini sudah digunakan'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data)->view('admin/changeemail', $data)->view('templates/footer');
        } else {
            $id = $data['user']['id_admin'];
            $nama = $data['user']['username'];
            $email = $this->input->post('email');
            $this->admin_model->gantiEmail($id, $email);

            $data = [
                'nama' => $nama,
                'mail' => $this->input->post('email'),
                'login' => "loggedin"
            ];
            $this->session->set_userdata($data);
            $this->session->set_flashdata('flash', 'Email berhasil diubah');
            redirect('admin/profil');
        }
    }
}
