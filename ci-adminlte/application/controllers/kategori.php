<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Kategori extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Kategori Produk";
        $data['user'] = $this->kategori_model->getUser($this->session->userdata('mail'));
        $data['kategori'] = $this->kategori_model->view();
        $this->form_validation->set_rules('jenis_produk', 'Jenis Produk', 'required');
        $this->form_validation->set_rules('kependekan', 'Kependekan', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data)->view('kategori/index', $data)->view('templates/footer');
        } else {
            if ($this->input->post('submit')) {
                $this->kategori_model->tambah();
                $this->session->set_flashdata('flash', 'Kategori berhasil ditambahkan');
                redirect('kategori');
            }
        }
    }
    public function changeActive()
    {
        $active = $this->input->post('active');
        $id_kategori = $this->input->post('idKategori');
        if ($active == 1) {
            $data = ['active' => 0];
            $this->db->where('id', $id_kategori)->update('t_kategori', $data);
            $this->session->set_flashdata('flash', 'Kategori berhasil diubah');
        } else {
            $data = ['active' => 1];
            $this->db->where('id', $id_kategori)->update('t_kategori', $data);
            $this->session->set_flashdata('flash', 'Kategori berhasil diubah');
        }
    }
    public function hapus($id)
    {
        if ($id > 3) {
            $this->kategori_model->delete($id);
            $this->session->set_flashdata('flash', 'Pesan berhasil dihapus');
            redirect('kategori');
        } else {
            redirect('kategori');
        }
    }
}
