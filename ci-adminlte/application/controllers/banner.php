<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Banner extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }

    public function index()
    {
        $data['judul'] = "Banner";
        $data['user'] = $this->banner_model->getUser($this->session->userdata('mail'));
        $data['gambar'] = $this->banner_model->view();
        $this->load->view('templates/header', $data)->view('banner/index', $data)->view('templates/footer');
    }
    public function tambah()
    {
        $data = array();
        $data['judul'] = "Banner";
        $data['subjudul'] = "Tambah Data";
        $data['user'] = $this->banner_model->getUser($this->session->userdata('mail'));
        if ($this->input->post('submit')) {
            $upload = $this->banner_model->upload();
            if ($upload['result'] == "success") {
                $this->banner_model->save($upload);
                $this->session->set_flashdata('flash', 'Data berhasil ditambahkan');
                redirect('banner');
            } else {
                redirect('banner/tambah');
            }
        }
        $this->load->view('templates/header', $data)->view('banner/tambah', $data)->view('templates/footer');
    }
    public function hapus($id)
    {
        $this->banner_model->delete($id);
        $this->session->set_flashdata('flash', 'Data berhasil dihapus');
        redirect('banner');
    }
}
