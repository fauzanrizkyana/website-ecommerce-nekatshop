<?php
defined('BASEPATH') or exit('No direct script access allowed');
class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "About";
        $data['user'] = $this->about_model->getUser($this->session->userdata('mail'));
        $data['about'] = $this->about_model->view();
        $this->load->view('templates/header', $data)->view('about/index', $data)->view('templates/footer');
    }
    public function edit($id_about)
    {
        $data['judul'] = 'About';
        $data['subjudul'] = "Ubah";
        $data['user'] = $this->about_model->getUser($this->session->userdata('mail'));
        $data['about'] = $this->about_model->getAboutById($id_about);

        if ($this->input->post('submit')) {
            $this->about_model->update();
            $this->session->set_flashdata('flash', 'Data berhasil diubah');
            redirect('about');
        }
        $this->load->view('templates/header', $data)->view('about/edit', $data)->view('templates/footer');
    }
}
