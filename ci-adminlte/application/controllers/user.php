<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "User";
        $data['user'] = $this->user_model->getUser($this->session->userdata('mail'));
        $data['member'] = $this->user_model->view();
        $this->load->view('templates/header', $data)->view('user/index', $data)->view('templates/footer');
    }
    public function detail($id)
    {
        $data['judul'] = "User";
        $data['subjudul'] = "Detail";
        $data['user'] = $this->user_model->getUser($this->session->userdata('mail'));
        $data['member'] = $this->user_model->getById($id);
        $this->load->view('templates/header', $data)->view('user/detail', $data)->view('templates/footer');
    }
}
