<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Dashboard";
        $data['user'] = $this->dashboard_model->getUser($this->session->userdata('mail'));
        $data['banner'] = $this->dashboard_model->countBanner();
        $data['admin'] = $this->dashboard_model->countAdmin();
        $data['member'] = $this->dashboard_model->countMember();
        $data['kategori'] = $this->dashboard_model->countKategori();
        $data['produk'] = $this->dashboard_model->countProduk();
        $data['berita'] = $this->dashboard_model->countBerita();
        $data['inbox'] = $this->dashboard_model->countInbox();
        $this->load->view('templates/header', $data)->view('dashboard/index', $data)->view('templates/footer');
    }
}
