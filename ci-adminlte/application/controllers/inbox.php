<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Inbox extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Inbox";
        $data['user'] = $this->inbox_model->getUser($this->session->userdata('mail'));
        $data['inbox'] = $this->inbox_model->view();
        $this->load->view('templates/header', $data)->view('inbox/index', $data)->view('templates/footer');
    }
    public function detail($id)
    {
        $data['judul'] = "Inbox";
        $data['subjudul'] = "Detail";
        $data['user'] = $this->inbox_model->getUser($this->session->userdata('mail'));
        $data['inbox'] = $this->inbox_model->getById($id);
        $this->load->view('templates/header', $data)->view('inbox/detail', $data)->view('templates/footer');
    }
    public function hapus($id)
    {
        $this->inbox_model->delete($id);
        $this->session->set_flashdata('flash', 'Pesan berhasil dihapus');
        redirect('inbox');
    }
    public function paymentConfirmation()
    {
        $data['judul'] = "Konfirmasi Pembayaran";
        $data['user'] = $this->inbox_model->getUser($this->session->userdata('mail'));
        $data['payment'] = $this->inbox_model->viewPaymentConfirmation();
        $this->load->view('templates/header', $data)->view('inbox/paymentconfirmation', $data)->view('templates/footer');
    }
    public function paymentDetail($id)
    {
        $data['judul'] = "Konfirmasi Pembayaran";
        $data['subjudul'] = "Detail";
        $data['user'] = $this->inbox_model->getUser($this->session->userdata('mail'));
        $data['payment'] = $this->inbox_model->getPaymentById($id);
        $this->load->view('templates/header', $data)->view('inbox/paymentdetail', $data)->view('templates/footer');
    }
}
