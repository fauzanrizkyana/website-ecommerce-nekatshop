<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Transaksi";
        $data['user'] = $this->transaksi_model->getUser($this->session->userdata('mail'));
        $data['transaksi'] = $this->transaksi_model->getTransaksi();
        $this->load->view('templates/header', $data)->view('transaksi/index')->view('templates/footer');
    }
    public function detail($id)
    {
        $data['judul'] = "Transaksi";
        $data['subjudul'] = "Detail";
        $data['user'] = $this->transaksi_model->getUser($this->session->userdata('mail'));
        $data['transaksi'] = $this->transaksi_model->getById($id);
        $data['item'] = $this->transaksi_model->getItemByCode($data['transaksi']['kode_order']);
        $this->load->view('templates/header', $data)->view('transaksi/detail', $data)->view('templates/footer');
    }
}
