<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			if ($this->session->userdata('login') == 'loggedin') {
				redirect('dashboard');
			} else {
				$data['judul'] = 'Login';
				$this->load->view('templates/auth_header', $data)->view('login/index')->view('templates/auth_footer');
			}
		} else {
			$this->_login();
		}
	}
	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$user = $this->login_model->getEmail($email);
		if ($user != null) {
			if (password_verify($password, $user['password'])) {
				$data = [
					'nama' => $user['nama'],
					'mail' => $user['email'],
					'login' => "loggedin"
				];
				$this->session->set_userdata($data);
				redirect('dashboard');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah</div>');
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email tidak registrasi</div>');
			redirect('login');
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('mail');
		$this->session->unset_userdata('login');
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda telah logout. Terima kasih!</div>');
		redirect('login');
	}
}
