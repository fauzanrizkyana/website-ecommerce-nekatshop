<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Produk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Produk";
        $kategori = $this->input->get('category');
        $data['user'] = $this->produk_model->getUser($this->session->userdata('mail'));
        if ($kategori) {
            $data['produk'] = $this->produk_model->viewByCategory($kategori);
        } else {
            $data['produk'] = $this->produk_model->view();
        }
        $this->load->view('templates/header', $data)->view('produk/index', $data)->view('templates/footer');
    }
    public function tambah()
    {
        $data = array();
        $data['judul'] = "Produk";
        $data['subjudul'] = "Tambah Data";
        $data['user'] = $this->produk_model->getUser($this->session->userdata('mail'));
        $data['jenis'] = $this->produk_model->getKategori();
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('deskripsi_produk', 'Deskripsi Produk', 'required');
        $this->form_validation->set_rules('harga_produk', 'Harga', 'required');
        $this->form_validation->set_rules('diskon', 'Diskon', 'numeric|less_than_equal_to[100]', [
            'less_than_equal_to' => 'Kelebihan diskon'
        ]);
        $this->form_validation->set_rules('stok_produk', 'Stok', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data)->view('produk/tambah')->view('templates/footer');
        } else {
            if ($this->input->post('submit')) {
                $upload = $this->produk_model->upload();

                if ($upload['result'] == "success") {
                    $this->produk_model->save($upload);
                    $this->session->set_flashdata('flash', 'Data produk berhasil ditambahkan');
                    redirect('produk');
                }
            }
        }
    }
    public function detail($id_produk)
    {
        $data['judul'] = 'Produk';
        $data['subjudul'] = "Detail";
        $data['user'] = $this->produk_model->getUser($this->session->userdata('mail'));
        $data['produk'] = $this->produk_model->getById($id_produk);
        $this->load->view('templates/header', $data)->view('produk/detail', $data)->view('templates/footer');
    }
    public function hapus($id_produk)
    {
        $this->produk_model->delete($id_produk);
        $this->session->set_flashdata('flash', 'Data produk berhasil dihapus');
        redirect('produk');
    }
    public function edit($id_produk)
    {
        $data['judul'] = 'Produk';
        $data['subjudul'] = "Ubah";
        $data['user'] = $this->produk_model->getUser($this->session->userdata('mail'));
        $data['produk'] = $this->produk_model->getById($id_produk);
        $data['jenis'] = $this->produk_model->getKategori();

        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('deskripsi_produk', 'Deskripsi Produk', 'required');
        $this->form_validation->set_rules('harga_produk', 'Harga', 'required');
        $this->form_validation->set_rules('diskon', 'Diskon', 'numeric|less_than_equal_to[100]', [
            'less_than_equal_to' => 'Kelebihan diskon'
        ]);
        $this->form_validation->set_rules('stok_produk', 'Stok', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data)->view('produk/edit', $data)->view('templates/footer');
        } else {
            if ($this->input->post('submit')) {
                $this->produk_model->update();
                $this->session->set_flashdata('flash', 'Data produk berhasil diubah');
                redirect('produk');
            }
        }
    }
}
