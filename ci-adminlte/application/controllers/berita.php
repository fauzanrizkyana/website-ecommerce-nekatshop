<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Berita extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('login') != "loggedin") {
            redirect('login');
        }
    }
    public function index()
    {
        $data['judul'] = "Berita";
        $data['user'] = $this->berita_model->getUser($this->session->userdata('mail'));
        $data['berita'] = $this->berita_model->view();
        $this->load->view('templates/header', $data)->view('berita/index')->view('templates/footer');
    }
    public function tambah()
    {
        $data['judul'] = "Berita";
        $data['subjudul'] = "Buat";
        $data['user'] = $this->berita_model->getUser($this->session->userdata('mail'));
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('penulis', 'Penulis', 'required');
        $this->form_validation->set_rules('isi_berita', 'Isi', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data)->view('berita/tambah')->view('templates/footer');
        } else {
            if ($this->input->post('submit')) {
                $upload = $this->berita_model->upload();

                if ($upload['result'] == "success") {
                    $this->berita_model->simpan($upload);
                    $this->session->set_flashdata('flash', 'Data berhasil ditambahkan');
                    redirect('berita');
                }
            }
        }
    }
    public function detail($id_berita)
    {
        $data['judul'] = 'Berita';
        $data['subjudul'] = "Detail";
        $data['user'] = $this->berita_model->getUser($this->session->userdata('mail'));
        $data['berita'] = $this->berita_model->getById($id_berita);
        $this->load->view('templates/header', $data)->view('berita/detail', $data)->view('templates/footer');
    }
    public function hapus($id_berita)
    {
        $this->berita_model->delete($id_berita);
        $this->session->set_flashdata('flash', 'Data berhasil dihapus');
        redirect('berita');
    }
    public function edit($id_berita)
    {
        $data['judul'] = 'berita';
        $data['subjudul'] = "Ubah";
        $data['user'] = $this->berita_model->getUser($this->session->userdata('mail'));
        $data['berita'] = $this->berita_model->getById($id_berita);
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('penulis', 'Penulis', 'required');
        $this->form_validation->set_rules('isi_berita', 'Isi', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data)->view('berita/edit', $data)->view('templates/footer');
        } else {
            if ($this->input->post('submit')) {
                $this->berita_model->update();
                $this->session->set_flashdata('flash', 'Data berhasil diubah');
                redirect('berita');
            }
        }
    }
}
