<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Inbox_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('t_feedback')->result_array();
    }
    public function getById($id)
    {
        return $this->db->get_where('t_feedback', ['id' => $id])->row();
    }
    public function delete($id)
    {
        $this->db->delete('t_feedback', ['id' => $id]);
    }
    public function viewPaymentConfirmation()
    {
        return $this->db->select('*')
            ->from('t_pembayaran')
            ->join('t_bank', 't_pembayaran.id_bank = t_bank.id_bank')
            ->order_by('t_pembayaran.tanggal_pembayaran', 'desc')
            ->get()->result_array();
    }
    public function getPaymentById($id)
    {
        return $this->db->select('*')
            ->from('t_pembayaran')
            ->join('t_bank', 't_pembayaran.id_bank = t_bank.id_bank')
            ->where(['t_pembayaran.id_pembayaran' => $id])
            ->get()->row_array();
    }
}
