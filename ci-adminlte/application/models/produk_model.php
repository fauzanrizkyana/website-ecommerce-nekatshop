<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Produk_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->get()->result_array();
    }
    public function viewByCategory($id_jenis_produk)
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_produk.id_jenis_produk' => $id_jenis_produk])
            ->get()->result_array();
    }
    public function getById($id_produk)
    {
        return $this->db->select('*')
            ->from('t_produk')
            ->join('t_kategori', 't_produk.id_jenis_produk = t_kategori.id')
            ->where(['t_produk.id_produk' => $id_produk])
            ->get()->row();
    }
    public function getKategori()
    {
        return $this->db->get('t_kategori')->result_array();
    }
    public function upload()
    {
        $config['upload_path'] = './assets/img2/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';
        $config['remove_space'] = TRUE;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar_produk')) {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    public function save($upload)
    {
        $data = array(
            'nama_produk' => $this->input->post('nama_produk', true),
            'id_jenis_produk' => $this->input->post('jenis_produk', true),
            'deskripsi_produk' => $this->input->post('deskripsi_produk', true),
            'harga_produk' => str_replace(".", "", $this->input->post('harga_produk', true)),
            'diskon' => $this->input->post('diskon', true),
            'stok_produk' => $this->input->post('stok_produk', true),
            'gambar_produk' => $upload['file']['file_name']
        );

        $this->db->insert('t_produk', $data);
    }

    public function update()
    {
        $id_produk =  $this->input->post('id_produk', true);
        $nama_produk = $this->input->post('nama_produk', true);
        $jenis_produk = $this->input->post('jenis_produk', true);
        $deskripsi_produk = $this->input->post('deskripsi_produk', true);
        $harga_produk = str_replace(".", "", $this->input->post('harga_produk', true));
        $diskon = $this->input->post('diskon', true);
        $stok_produk = $this->input->post('stok_produk', true);

        $config['upload_path'] = './assets/img2/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar_produk')) {
            $upload_data = $this->upload->data();
            $gambar_produk = $upload_data['file_name'];
            $gambar_produk_lama = $this->input->post('gambar_produk_lama', true);
            unlink(FCPATH . "assets/img2/" . $gambar_produk_lama);
        } else {
            $gambar_produk = $this->input->post('gambar_produk_lama', true);
        }

        $data = [
            'nama_produk' => $nama_produk,
            'id_jenis_produk' => $jenis_produk,
            'deskripsi_produk' => $deskripsi_produk,
            'harga_produk' => $harga_produk,
            'diskon' => $diskon,
            'stok_produk' => $stok_produk,
            'gambar_produk' => $gambar_produk
        ];

        $this->db->where('id_produk', $id_produk)->update('t_produk', $data);
    }
    public function delete($id_produk)
    {
        $_id = $this->db->get_where('t_produk', ['id_produk' => $id_produk])->row();
        $query = $this->db->delete('t_produk', ['id_produk' => $id_produk]);
        if ($query) {
            unlink("./assets/img2/" . $_id->gambar_produk);
        }
    }
}
