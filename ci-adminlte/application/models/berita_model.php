<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Berita_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        return $this->db->get('t_berita')->result_array();
    }
    public function getById($id_berita)
    {
        return $this->db->get_where('t_berita', ['id_berita' => $id_berita])->row();
    }
    public function upload()
    {
        $config['upload_path'] = './assets/img3/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';
        $config['remove_space'] = TRUE;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }
    public function simpan($upload)
    {
        date_default_timezone_set('Asia/Jakarta');
        $data = array(
            'judul' => $this->input->post('judul', true),
            'tanggal' => time(),
            'penulis' => $this->input->post('penulis', true),
            'isi_berita' => $this->input->post('isi_berita', true),
            'gambar' => $upload['file']['file_name']
        );

        $this->db->insert('t_berita', $data);
    }
    public function update()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id_berita =  $this->input->post('id_berita', true);
        $judul = $this->input->post('judul', true);
        $tanggal = $this->input->post('tanggal', true);
        $penulis = $this->input->post('penulis', true);
        $isi_berita = $this->input->post('isi_berita', true);

        $config['upload_path'] = './assets/img3/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $upload_data = $this->upload->data();
            $gambar = $upload_data['file_name'];
            $gambar_lama = $this->input->post('gambar_lama', true);
            unlink(FCPATH . "assets/img3/" . $gambar_lama);
        } else {
            $gambar = $this->input->post('gambar_lama', true);
        }

        $data = [
            'judul' => $judul,
            'tanggal' => $tanggal,
            'penulis' => $penulis,
            'isi_berita' => $isi_berita,
            'gambar' => $gambar
        ];

        $this->db->where('id_berita', $id_berita)->update('t_berita', $data);
    }
    public function delete($id_berita)
    {
        $_id = $this->db->get_where('t_berita', ['id_berita' => $id_berita])->row();
        $query = $this->db->delete('t_berita', ['id_berita' => $id_berita]);
        if ($query) {
            unlink(FCPATH . "assets/img3/" . $_id->gambar);
        }
    }
}
