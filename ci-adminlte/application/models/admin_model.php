<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        return $this->db->get('t_admin')->result_array();
    }
    public function getById($id_admin)
    {
        return $this->db->get_where('t_admin', ['id_admin' => $id_admin])->row();
    }
    public function tambah()
    {
        $data = [
            'username' => htmlspecialchars($this->input->post('username', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'image' => 'default.jpg',
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'is_active' => 1,
            'date_created' => time()
        ];
        $this->db->insert('t_admin', $data);
    }
    public function delete($id_admin)
    {
        $_id = $this->db->get_where('t_admin', ['id_admin' => $id_admin])->row();
        $query = $this->db->delete('t_admin', ['id_admin' => $id_admin]);
        if ($query) {
            if ($_id != 'default.jpg') {
                unlink(FCPATH . 'assets/img5/' . $_id->gambar_produk);
            }
        }
    }
    public function ubahProfil($gambar_lama)
    {
        $username = $this->input->post('username');
        $email = $this->input->post('email');

        $upload_image = $_FILES['image']['name'];
        if ($upload_image) {
            $config['upload_path'] = './assets/img5/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['remove_space'] = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $old_image = $gambar_lama;
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'assets/img5/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
                $this->db->set('image', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('username', $username);
        $this->db->where('email', $email);
        $this->db->update('t_admin');
    }
    public function gantiPassword($new_password, $email)
    {
        $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
        $this->db->set('password', $password_hash);
        $this->db->where('email', $email);
        $this->db->update('t_admin');
    }
    public function gantiEmail($id, $email)
    {
        $this->db->where('id_admin', $id)->update('t_admin', ['email' => $email]);
    }
}
