<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Banner_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        return $this->db->get('t_banner')->result();
    }

    public function upload()
    {
        $config['upload_path'] = './assets/img/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';
        $config['remove_space'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('input_gambar')) {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    public function save($upload)
    {
        $data = array(
            'nama_file' => $upload['file']['file_name'],
            'ukuran_file' => $upload['file']['file_size'],
            'tipe_file' => $upload['file']['file_type']
        );

        $this->db->insert('t_banner', $data);
    }
    public function delete($id)
    {
        $_id = $this->db->get_where('t_banner', ['id' => $id])->row();
        $query = $this->db->delete('t_banner', ['id' => $id]);
        if ($query) {
            unlink(FCPATH . "assets/img/" . $_id->nama_file);
        }
    }
}
