<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaksi_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function getTransaksi()
    {
        return $this->db->select('*')
            ->from('t_order')
            ->join('t_user', 't_order.email = t_user.email')
            ->join('t_provinsi', 't_order.id_provinsi = t_provinsi.id_provinsi')
            ->join('t_kota', 't_order.id_kota = t_kota.id_kota')
            ->order_by('t_order.tanggal_order', 'desc')
            ->get()->result_array();
    }
    public function getById($id)
    {
        return $this->db->select('*')
            ->from('t_order')
            ->join('t_user', 't_order.email = t_user.email')
            ->join('t_provinsi', 't_order.id_provinsi = t_provinsi.id_provinsi')
            ->join('t_kota', 't_order.id_kota = t_kota.id_kota')
            ->where(['t_order.id_order' => $id])
            ->get()->row_array();
    }
    public function getItemByCode($kode_order)
    {
        return $this->db->select('*')
            ->from('t_barang_order')
            ->join('t_produk', 't_barang_order.id_produk = t_produk.id_produk')
            ->where(['t_barang_order.kode_order' => $kode_order])
            ->get()->result_array();
    }
}
