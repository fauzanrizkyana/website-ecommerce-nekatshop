<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function countBanner()
    {
        return $this->db->count_all_results('t_banner');
    }
    public function countAdmin()
    {
        return $this->db->count_all_results('t_admin');
    }
    public function countMember()
    {
        return $this->db->count_all_results('t_user');
    }
    public function countKategori()
    {
        return $this->db->count_all_results('t_kategori');
    }
    public function countProduk()
    {
        return $this->db->count_all_results('t_produk');
    }
    public function countBerita()
    {
        return $this->db->count_all_results('t_berita');
    }
    public function countInbox()
    {
        return $this->db->count_all_results('t_feedback');
    }
}
