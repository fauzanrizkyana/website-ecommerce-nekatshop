<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        return $this->db->get('t_user')->result_array();
    }
    public function getById($id)
    {
        return $this->db->select('*')
            ->from('t_user')
            ->join('t_jenis_kelamin', 't_user.id_jenis_kelamin = t_jenis_kelamin.id')
            ->where(['t_user.id_user' => $id])
            ->get()->row_array();
    }
}
