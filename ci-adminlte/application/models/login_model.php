<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login_Model extends CI_Model
{
	public function getEmail($email)
	{
		return $this->db->get_where('t_admin', ['email' => $email])->row_array();
	}
	public function getByCookie($cookie)
	{
		return $this->db->get_where('t_admin', ["cookie" => $cookie])->row();
	}
	public function updateCookie($key, $id_admin)
	{
		$data = ["cookie" => $key];
		$this->db->where('id_admin', $id_admin)->update('t_admin', $data);
	}
}
