<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Kategori_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }
    public function view()
    {
        return $this->db->get('t_kategori')->result_array();
    }
    public function tambah()
    {
        $jenis_produk = $this->input->post('jenis_produk');
        $kependekan = $this->input->post('kependekan');
        if ($this->input->post('active')) {
            $active = 1;
        } else {
            $active = 0;
        }
        $data = [
            'jenis_produk' => $jenis_produk,
            'kependekan' => $kependekan,
            'active' => $active
        ];
        $this->db->insert('t_kategori', $data);
    }
    public function delete($id)
    {
        $this->db->delete('t_kategori', ['id' => $id]);
    }
}
