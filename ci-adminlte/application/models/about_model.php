<?php
defined('BASEPATH') or exit('No direct script access allowed');
class About_Model extends CI_Model
{
    public function getUser($email)
    {
        return $this->db->get_where('t_admin', ['email' => $email])->row_array();
    }

    public function view()
    {
        return $this->db->get('t_about')->result_array();
    }
    public function getAboutById($id_about)
    {
        return $this->db->get_where('t_about', ['id_about' => $id_about])->row();
    }
    public function update()
    {
        $id_about =  $this->input->post('id_about', true);
        $isi_about = $this->input->post('isi_about', true);

        $config['upload_path'] = './assets/img4/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar_about')) {
            $upload_data = $this->upload->data();
            $gambar_about = $upload_data['file_name'];
            $gambar_about_lama = $this->input->post('gambar_about_lama', true);
            unlink(FCPATH . "assets/img4/" . $gambar_about_lama);
        } else {
            $gambar_about = $this->input->post('gambar_about_lama', true);
        }

        $data = [
            'isi_about' => $isi_about,
            'gambar_about' => $gambar_about
        ];

        $this->db->where('id_about', $id_about)->update('t_about', $data);
    }
}
