-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2019 at 05:15 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_about`
--

CREATE TABLE `t_about` (
  `id_about` int(11) NOT NULL,
  `isi_about` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_about` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_about`
--

INSERT INTO `t_about` (`id_about`, `isi_about`, `gambar_about`) VALUES
(1, '                                                                                                                        <p xss=\"removed\">Nekatshop merupakan sebuah situs ecommerce terbesar dan terbaik di kecamatan Katapang. Nekatshop berdiri sekitar pada tahun 2019 yang dibuat oleh anak muda katapang bandung, jawa barat.</p><p xss=\"removed\">Situs ini menjual berbagai produk merek pakaian dan alas kaki  dari dalam dan luar negeri. Situs ini dikerjakan oleh lebih dari 100 karyawan dan 20 admin. Nekatshop dijamin memiliki garansi hingga 100% untuk semua produk. Nekatshop, the best e-commerce in katapang.</p>                                                                                                                                                                ', 'undraw_online_shopping_ga731.png');

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`id_admin`, `username`, `email`, `image`, `password`, `is_active`, `date_created`) VALUES
(5, 'Sandhika Galih', 'sandhikagalih@gmail.com', 'IMG_20191023_212656_621.jpg', '$2y$10$4MfsODl4t6xpiy.EZNjSwetRA2eqjwLa8UQcpKoudJP6ZEFw9CIvG', 1, 1573463272),
(6, 'Fauzan Rizkyana. G', 'fauzanrizkyana@gmail.com', 'user2-160x160.jpg', '$2y$10$mBowe9sTpoCoWv6gpZwq0urg6KGzHX4FzaWUakKmdseGQm4yeAiVa', 1, 1573782237),
(7, 'Doddy Ferdiansyah', 'doddy@gmail.com', 'user1-128x128.jpg', '$2y$10$CDmIqq9ggjN4X3Uy51l8.et2DfQtlyRlf2/O1qOXCFt/IgNYp8ty.', 1, 1574067360),
(8, 'Nofariza Handayani', 'nofa@yahoo.com', 'default.jpg', '$2y$10$QtaEmB1SXM5ZPrrZbxVHgOtJl7Hq40RDsG98D/LKWi5necC6sy7k2', 1, 1574994760),
(9, 'Super Admin', 'admin@nekatshop.com', 'default.jpg', '$2y$10$iTrlPtczfhzE5UDyevQBV.mRSLXm/vz3.CJOyatINfZuwULS5kxaW', 1, 1575015068),
(10, 'Viral Dong', 'viraldong21@gmail.com', 'default.jpg', '$2y$10$37mrAJ49uZ2sh55V5DLBeOdfhNqQ4Zwkcnbv8h/02RJT4TPoTRxj6', 1, 1577155808);

-- --------------------------------------------------------

--
-- Table structure for table `t_bank`
--

CREATE TABLE `t_bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_bank`
--

INSERT INTO `t_bank` (`id_bank`, `nama_bank`) VALUES
(1, 'Bank Mandiri'),
(2, 'BNI'),
(3, 'BRI'),
(4, 'BTN'),
(5, 'BCA'),
(6, 'Bank Mega'),
(7, 'Bank Bukopin'),
(8, 'Bank CIMB Niaga'),
(9, 'Bank Danamon'),
(10, 'Bank OCBC NISP'),
(11, 'Permata Bank'),
(12, 'Bank Citibank'),
(13, 'Bank Maybank Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `t_banner`
--

CREATE TABLE `t_banner` (
  `id` int(11) NOT NULL,
  `nama_file` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ukuran_file` double NOT NULL,
  `tipe_file` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_banner`
--

INSERT INTO `t_banner` (`id`, `nama_file`, `ukuran_file`, `tipe_file`) VALUES
(17, 'banner1.JPG', 68.71, 'image/jpeg'),
(18, 'PROMO_SPESIAL_DI_AKHIR_BULAN!.png', 34.09, 'image/png'),
(19, 'image_6.jpg', 99.61, 'image/jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `t_barang_order`
--

CREATE TABLE `t_barang_order` (
  `id_barang_order` int(11) NOT NULL,
  `kode_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_produk` int(11) NOT NULL,
  `ukuran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_barang_order`
--

INSERT INTO `t_barang_order` (`id_barang_order`, `kode_order`, `id_produk`, `ukuran`, `harga_satuan`, `qty`, `harga_total`) VALUES
(1, '20191228C27MQXUEQVKLJ9D8', 32, 'M', 92000, 1, 92000);

-- --------------------------------------------------------

--
-- Table structure for table `t_berita`
--

CREATE TABLE `t_berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` int(11) NOT NULL,
  `penulis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_berita` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_berita`
--

INSERT INTO `t_berita` (`id_berita`, `judul`, `tanggal`, `penulis`, `isi_berita`, `gambar`) VALUES
(4, 'Cara aman membayar menggunakan rekening bank', 1574215000, 'Admin', '                                                                                <p xss=\"removed\">Terkadang jika mau membeli sebuah barang di toko online, pasti membayar menggunakan tunai. Selain itu, juga bisa menggunakan rekening bank. Namun bagaimana caranya agar aman membayar menggunakan rekening bank? Saya akan memberikan tips berikut:</p><ol><li>Jangan memberitahukan kata sandi kartu kredit kepada orang yang tidak kenal</li><li>Gunakan rekening bank yang sewajarnya</li><li>Selalu hati-hati jika ingin mentransfer uang ke rekening bank para pemilik toko</li></ol>                                                                                ', 'blog-2.jpg'),
(6, 'Nekatshop hadir untuk anda', 1574215550, 'Erik', '                                                            <p xss=\"removed\">Nekatshop telah resmi diluncurkan pada tanggal 15 Oktober 2019 lalu. Pada perluncuran website ini diketuai oleh Mamat Suparman, CEO Nekatshop bersama dengan para penjabat serta tamu lainnya. Perluncuran ini diawali dengan penandatanganan pada sebuah sertifikat kemudian mengucapkan resmi diluncurkan sebagai tanda diluncurkannya website e-commerce Nekatshop.</p>                                                            ', 'hero_2.jpg'),
(7, 'Toko nekatshop hadir di kota anda', 1574216050, 'Fery', '                                                        <p xss=\"removed\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed purus est, dictum vel mollis sit amet, sodales a leo. Maecenas rutrum quam a massa lobortis scelerisque. Proin tincidunt nisl risus, ac tincidunt ante rutrum et. Pellentesque cursus rutrum nisl, a scelerisque augue volutpat non. Morbi blandit mattis pellentesque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam pretium viverra sem sit amet egestas. Phasellus quis varius ex. Proin id risus at lacus porta tincidunt.</p><p xss=\"removed\">Phasellus ultricies neque non nisl malesuada sollicitudin. Nunc bibendum sollicitudin felis vel laoreet. Sed ac mi nibh. Morbi vitae ex tempus, blandit orci eget, interdum felis. Phasellus et imperdiet velit, sit amet elementum nunc. Nunc eget convallis quam. Vestibulum ac finibus libero, ac euismod sem. Suspendisse rutrum lorem nec enim euismod dictum. Donec consectetur maximus mauris, in aliquet purus porttitor sed. Nam eu nisl vestibulum, suscipit erat non, varius lorem. Quisque scelerisque ultrices tempor. Ut gravida nibh ut orci blandit aliquam. Nullam id sem pulvinar, sodales mi vitae, pellentesque est. Donec posuere, velit in molestie porttitor, orci lectus dictum ex, sed scelerisque lectus elit a diam. Morbi eros magna, aliquet non tristique at, aliquam non felis.</p>                                                      ', 'about.jpg'),
(8, 'Cara Belanja', 1575862472, 'Admin', '                                                                                                                                                                                    <p xss=\"removed\">Berikut adalah prosedur belanja di toko kami, mohon dibaca dengan teliti dan jelas, sehingga tidak ada pihak yang merasa dirugikan karena tidak faham dengan prosedur yang ada, dan apabila ada ketentuan yang kurang jelas, bisa ditanyakan langsung ke customer service kami.</p><p xss=\"removed\">1.Silahkan pilih barang yang anda butuhkan, anda bisa menemukan barang yang anda maksud dengan mengklik menu daftar barang, atau mencari berdasarkan kategori atau berdasarkan nama pada kolom cari yang sudah disediakan.</p><p xss=\"removed\">2.Jika barang yang anda maksud sudah ditemukan, silahkan anda menentukan pilihan, apakah langsung menambahkan ke dalam keranjang belanja, atau melihat detail dari barang tersebut, dimana didalam detail akan diapaprkan deskripsi barang, mulai dari stok yang tersedia, jenis barang, bahan dasar, serta penjelasan secara lebih rinci.</p><p xss=\"removed\">3.Jika sudah yakin dengan pilihan anda, silahkan klik menu keranjang untuk melihat detail belanja anda, jika anda ingin menambah jumlah item barang yang anda maksud silahkan ubah angka yang ada di texbox dengan jumlah yang anda inginkan dan kemudian klik button update untuk memperbaharui data belanjaan anda, dengan catatan tidak melebihi stok yang ada.</p><p xss=\"removed\">4.Jika anda masih ingin belanja barang yang lain anda bisa mengklik button lanjutkan belanja, atau jika sudah yakin dan tidak ingin berbelanja lagi, silahkan klik button selesai belanja untuk proses pengisian data anda.</p><p xss=\"removed\">5.Silahkan isi data lengkap anda sesuai dengan yang diminta, dan jika sudah yakin data anda benar, silahkan klik button krirm untuk melanjutkan pesanan anda ke customer service kami, dan disaat bersamaan juga anda akan mendapatkan feedback dari kami berupa detail keseluruhan belanja beserta kode transaksi anda, silahkan catat kode transaksi yang diberikan oleh pihak kami, dan  kode ini penting untuk memproses transaksi anda selanjutnya.</p><p xss=\"removed\">6.Setelah anda melakukan transfer sesuai jumlah yang ditentukan dan kemudian mengkonfirmasi pembayaran anda ke pada pihak kami, lengkap dengan bukti transfer yang anda miliki, bukti yang otentik yang kami minta adalah capture dari bukti pembayaran anda ke rekening kami, baik melalui sms banking, ATM, internet banking atau bentuk lainnya.</p><p xss=\"removed\">7.Barang yang anda minta akan segera kami proses 1x24 jam setelah anda mengikuti semua prosedur yang kami minta, jika dalam waktu 3x 24 jam anda tidak melakukan konfirmasi pembayaran, pesanan anda kami anggap dibatalkan,</p><p xss=\"removed\">Dengan membaca ketentuan ini diharapkan anda sudah faham dan menyetujui ketentuan yang telah kami terapkan, terima kasih.</p>                                                                                                                                                                                                                                    ', 'blog-3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_feedback`
--

CREATE TABLE `t_feedback` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` int(11) NOT NULL,
  `subjek` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_kelamin`
--

CREATE TABLE `t_jenis_kelamin` (
  `id` int(11) NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_jenis_kelamin`
--

INSERT INTO `t_jenis_kelamin` (`id`, `jenis_kelamin`) VALUES
(1, 'Pria'),
(2, 'Wanita');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori`
--

CREATE TABLE `t_kategori` (
  `id` int(11) NOT NULL,
  `jenis_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kependekan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_kategori`
--

INSERT INTO `t_kategori` (`id`, `jenis_produk`, `kependekan`, `active`) VALUES
(1, 'Pakaian Pria', 'P. Pria', 1),
(2, 'Pakaian Wanita', 'P. Wanita', 1),
(3, 'Alas Kaki', 'Alas Kaki', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kota`
--

CREATE TABLE `t_kota` (
  `id_kota` bigint(20) UNSIGNED NOT NULL,
  `id_provinsi` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kota` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_kota`
--

INSERT INTO `t_kota` (`id_kota`, `id_provinsi`, `type`, `nama_kota`, `kode_pos`) VALUES
(1, 21, 'Kabupaten', 'Kab. Aceh Barat', '23681'),
(2, 21, 'Kabupaten', 'Kab. Aceh Barat Daya', '23764'),
(3, 21, 'Kabupaten', 'Kab. Aceh Besar', '23951'),
(4, 21, 'Kabupaten', 'Kab. Aceh Jaya', '23654'),
(5, 21, 'Kabupaten', 'Kab. Aceh Selatan', '23719'),
(6, 21, 'Kabupaten', 'Kab. Aceh Singkil', '24785'),
(7, 21, 'Kabupaten', 'Kab. Aceh Tamiang', '24476'),
(8, 21, 'Kabupaten', 'Kab. Aceh Tengah', '24511'),
(9, 21, 'Kabupaten', 'Kab. Aceh Tenggara', '24611'),
(10, 21, 'Kabupaten', 'Kab. Aceh Timur', '24454'),
(11, 21, 'Kabupaten', 'Kab. Aceh Utara', '24382'),
(12, 32, 'Kabupaten', 'Kab. Agam', '26411'),
(13, 23, 'Kabupaten', 'Kab. Alor', '85811'),
(14, 19, 'Kota', 'Ambon', '97222'),
(15, 34, 'Kabupaten', 'Kab. Asahan', '21214'),
(16, 24, 'Kabupaten', 'Kab. Asmat', '99777'),
(17, 1, 'Kabupaten', 'Kab. Badung', '80351'),
(18, 13, 'Kabupaten', 'Kab. Balangan', '71611'),
(19, 15, 'Kota', 'Balikpapan', '76111'),
(20, 21, 'Kota', 'Banda Aceh', '23238'),
(21, 18, 'Kota', 'Bandar Lampung', '35139'),
(22, 9, 'Kabupaten', 'Kab. Bandung', '40311'),
(23, 9, 'Kota', 'Bandung', '40111'),
(24, 9, 'Kabupaten', 'Kab. Bandung Barat', '40721'),
(25, 29, 'Kabupaten', 'Kab. Banggai', '94711'),
(26, 29, 'Kabupaten', 'Kab. Banggai Kepulauan', '94881'),
(27, 2, 'Kabupaten', 'Kab. Bangka', '33212'),
(28, 2, 'Kabupaten', 'Kab. Bangka Barat', '33315'),
(29, 2, 'Kabupaten', 'Kab. Bangka Selatan', '33719'),
(30, 2, 'Kabupaten', 'Kab. Bangka Tengah', '33613'),
(31, 11, 'Kabupaten', 'Kab. Bangkalan', '69118'),
(32, 1, 'Kabupaten', 'Kab. Bangli', '80619'),
(33, 13, 'Kabupaten', 'Kab. Banjar', '70619'),
(34, 9, 'Kota', 'Banjar', '46311'),
(35, 13, 'Kota', 'Banjarbaru', '70712'),
(36, 13, 'Kota', 'Banjarmasin', '70117'),
(37, 10, 'Kabupaten', 'Kab. Banjarnegara', '53419'),
(38, 28, 'Kabupaten', 'Kab. Bantaeng', '92411'),
(39, 5, 'Kabupaten', 'Kab. Bantul', '55715'),
(40, 33, 'Kabupaten', 'Kab. Banyuasin', '30911'),
(41, 10, 'Kabupaten', 'Kab. Banyumas', '53114'),
(42, 11, 'Kabupaten', 'Kab. Banyuwangi', '68416'),
(43, 13, 'Kabupaten', 'Kab. Barito Kuala', '70511'),
(44, 14, 'Kabupaten', 'Kab. Barito Selatan', '73711'),
(45, 14, 'Kabupaten', 'Kab. Barito Timur', '73671'),
(46, 14, 'Kabupaten', 'Kab. Barito Utara', '73881'),
(47, 28, 'Kabupaten', 'Kab. Barru', '90719'),
(48, 17, 'Kota', 'Batam', '29413'),
(49, 10, 'Kabupaten', 'Kab. Batang', '51211'),
(50, 8, 'Kabupaten', 'Kab. Batang Hari', '36613'),
(51, 11, 'Kota', 'Batu', '65311'),
(52, 34, 'Kabupaten', 'Kab. Batu Bara', '21655'),
(53, 30, 'Kota', 'Bau-Bau', '93719'),
(54, 9, 'Kabupaten', 'Kab. Bekasi', '17837'),
(55, 9, 'Kota', 'Bekasi', '17121'),
(56, 2, 'Kabupaten', 'Kab. Belitung', '33419'),
(57, 2, 'Kabupaten', 'Kab. Belitung Timur', '33519'),
(58, 23, 'Kabupaten', 'Kab. Belu', '85711'),
(59, 21, 'Kabupaten', 'Kab. Bener Meriah', '24581'),
(60, 26, 'Kabupaten', 'Kab. Bengkalis', '28719'),
(61, 12, 'Kabupaten', 'Kab. Bengkayang', '79213'),
(62, 4, 'Kota', 'Bengkulu', '38229'),
(63, 4, 'Kabupaten', 'Kab. Bengkulu Selatan', '38519'),
(64, 4, 'Kabupaten', 'Kab. Bengkulu Tengah', '38319'),
(65, 4, 'Kabupaten', 'Kab. Bengkulu Utara', '38619'),
(66, 15, 'Kabupaten', 'Kab. Berau', '77311'),
(67, 24, 'Kabupaten', 'Kab. Biak Numfor', '98119'),
(68, 22, 'Kabupaten', 'Kab. Bima', '84171'),
(69, 22, 'Kota', 'Bima', '84139'),
(70, 34, 'Kota', 'Binjai', '20712'),
(71, 17, 'Kabupaten', 'Kab. Bintan', '29135'),
(72, 21, 'Kabupaten', 'Kab. Bireuen', '24219'),
(73, 31, 'Kota', 'Bitung', '95512'),
(74, 11, 'Kabupaten', 'Kab. Blitar', '66171'),
(75, 11, 'Kota', 'Blitar', '66124'),
(76, 10, 'Kabupaten', 'Kab. Blora', '58219'),
(77, 7, 'Kabupaten', 'Kab. Boalemo', '96319'),
(78, 9, 'Kabupaten', 'Kab. Bogor', '16911'),
(79, 9, 'Kota', 'Bogor', '16119'),
(80, 11, 'Kabupaten', 'Kab. Bojonegoro', '62119'),
(81, 31, 'Kabupaten', 'Kab. Bolaang Mongondow (Bolmong)', '95755'),
(82, 31, 'Kabupaten', 'Kab. Bolaang Mongondow Selatan', '95774'),
(83, 31, 'Kabupaten', 'Kab. Bolaang Mongondow Timur', '95783'),
(84, 31, 'Kabupaten', 'Kab. Bolaang Mongondow Utara', '95765'),
(85, 30, 'Kabupaten', 'Kab. Bombana', '93771'),
(86, 11, 'Kabupaten', 'Kab. Bondowoso', '68219'),
(87, 28, 'Kabupaten', 'Kab. Bone', '92713'),
(88, 7, 'Kabupaten', 'Kab. Bone Bolango', '96511'),
(89, 15, 'Kota', 'Bontang', '75313'),
(90, 24, 'Kabupaten', 'Kab. Boven Digoel', '99662'),
(91, 10, 'Kabupaten', 'Kab. Boyolali', '57312'),
(92, 10, 'Kabupaten', 'Kab. Brebes', '52212'),
(93, 32, 'Kota', 'Bukittinggi', '26115'),
(94, 1, 'Kabupaten', 'Kab. Buleleng', '81111'),
(95, 28, 'Kabupaten', 'Kab. Bulukumba', '92511'),
(96, 16, 'Kabupaten', 'Kab. Bulungan (Bulongan)', '77211'),
(97, 8, 'Kabupaten', 'Kab. Bungo', '37216'),
(98, 29, 'Kabupaten', 'Kab. Buol', '94564'),
(99, 19, 'Kabupaten', 'Kab. Buru', '97371'),
(100, 19, 'Kabupaten', 'Kab. Buru Selatan', '97351'),
(101, 30, 'Kabupaten', 'Kab. Buton', '93754'),
(102, 30, 'Kabupaten', 'Kab. Buton Utara', '93745'),
(103, 9, 'Kabupaten', 'Kab. Ciamis', '46211'),
(104, 9, 'Kabupaten', 'Kab. Cianjur', '43217'),
(105, 10, 'Kabupaten', 'Kab. Cilacap', '53211'),
(106, 3, 'Kota', 'Cilegon', '42417'),
(107, 9, 'Kota', 'Cimahi', '40512'),
(108, 9, 'Kabupaten', 'Kab. Cirebon', '45611'),
(109, 9, 'Kota', 'Cirebon', '45116'),
(110, 34, 'Kabupaten', 'Kab. Dairi', '22211'),
(111, 24, 'Kabupaten', 'Kab. Deiyai (Deliyai)', '98784'),
(112, 34, 'Kabupaten', 'Kab. Deli Serdang', '20511'),
(113, 10, 'Kabupaten', 'Kab. Demak', '59519'),
(114, 1, 'Kota', 'Denpasar', '80227'),
(115, 9, 'Kota', 'Depok', '16416'),
(116, 32, 'Kabupaten', 'Kab. Dharmasraya', '27612'),
(117, 24, 'Kabupaten', 'Kab. Dogiyai', '98866'),
(118, 22, 'Kabupaten', 'Kab. Dompu', '84217'),
(119, 29, 'Kabupaten', 'Kab. Donggala', '94341'),
(120, 26, 'Kota', 'Dumai', '28811'),
(121, 33, 'Kabupaten', 'Kab. Empat Lawang', '31811'),
(122, 23, 'Kabupaten', 'Kab. Ende', '86351'),
(123, 28, 'Kabupaten', 'Kab. Enrekang', '91719'),
(124, 25, 'Kabupaten', 'Kab. Fakfak', '98651'),
(125, 23, 'Kabupaten', 'Kab. Flores Timur', '86213'),
(126, 9, 'Kabupaten', 'Kab. Garut', '44126'),
(127, 21, 'Kabupaten', 'Kab. Gayo Lues', '24653'),
(128, 1, 'Kabupaten', 'Kab. Gianyar', '80519'),
(129, 7, 'Kabupaten', 'Kab. Gorontalo', '96218'),
(130, 7, 'Kota', 'Gorontalo', '96115'),
(131, 7, 'Kabupaten', 'Kab. Gorontalo Utara', '96611'),
(132, 28, 'Kabupaten', 'Kab. Gowa', '92111'),
(133, 11, 'Kabupaten', 'Kab. Gresik', '61115'),
(134, 10, 'Kabupaten', 'Kab. Grobogan', '58111'),
(135, 5, 'Kabupaten', 'Kab. Gunung Kidul', '55812'),
(136, 14, 'Kabupaten', 'Kab. Gunung Mas', '74511'),
(137, 34, 'Kota', 'Gunungsitoli', '22813'),
(138, 20, 'Kabupaten', 'Kab. Halmahera Barat', '97757'),
(139, 20, 'Kabupaten', 'Kab. Halmahera Selatan', '97911'),
(140, 20, 'Kabupaten', 'Kab. Halmahera Tengah', '97853'),
(141, 20, 'Kabupaten', 'Kab. Halmahera Timur', '97862'),
(142, 20, 'Kabupaten', 'Kab. Halmahera Utara', '97762'),
(143, 13, 'Kabupaten', 'Kab. Hulu Sungai Selatan', '71212'),
(144, 13, 'Kabupaten', 'Kab. Hulu Sungai Tengah', '71313'),
(145, 13, 'Kabupaten', 'Kab. Hulu Sungai Utara', '71419'),
(146, 34, 'Kabupaten', 'Kab. Humbang Hasundutan', '22457'),
(147, 26, 'Kabupaten', 'Kab. Indragiri Hilir', '29212'),
(148, 26, 'Kabupaten', 'Kab. Indragiri Hulu', '29319'),
(149, 9, 'Kabupaten', 'Kab. Indramayu', '45214'),
(150, 24, 'Kabupaten', 'Kab. Intan Jaya', '98771'),
(151, 6, 'Kota', 'Jakarta Barat', '11220'),
(152, 6, 'Kota', 'Jakarta Pusat', '10540'),
(153, 6, 'Kota', 'Jakarta Selatan', '12230'),
(154, 6, 'Kota', 'Jakarta Timur', '13330'),
(155, 6, 'Kota', 'Jakarta Utara', '14140'),
(156, 8, 'Kota', 'Jambi', '36111'),
(157, 24, 'Kabupaten', 'Kab. Jayapura', '99352'),
(158, 24, 'Kota', 'Jayapura', '99114'),
(159, 24, 'Kabupaten', 'Kab. Jayawijaya', '99511'),
(160, 11, 'Kabupaten', 'Kab. Jember', '68113'),
(161, 1, 'Kabupaten', 'Kab. Jembrana', '82251'),
(162, 28, 'Kabupaten', 'Kab. Jeneponto', '92319'),
(163, 10, 'Kabupaten', 'Kab. Jepara', '59419'),
(164, 11, 'Kabupaten', 'Kab. Jombang', '61415'),
(165, 25, 'Kabupaten', 'Kab. Kaimana', '98671'),
(166, 26, 'Kabupaten', 'Kab. Kampar', '28411'),
(167, 14, 'Kabupaten', 'Kab. Kapuas', '73583'),
(168, 12, 'Kabupaten', 'Kab. Kapuas Hulu', '78719'),
(169, 10, 'Kabupaten', 'Kab. Karanganyar', '57718'),
(170, 1, 'Kabupaten', 'Kab. Karangasem', '80819'),
(171, 9, 'Kabupaten', 'Kab. Karawang', '41311'),
(172, 17, 'Kabupaten', 'Kab. Karimun', '29611'),
(173, 34, 'Kabupaten', 'Kab. Karo', '22119'),
(174, 14, 'Kabupaten', 'Kab. Katingan', '74411'),
(175, 4, 'Kabupaten', 'Kab. Kaur', '38911'),
(176, 12, 'Kabupaten', 'Kab. Kayong Utara', '78852'),
(177, 10, 'Kabupaten', 'Kab. Kebumen', '54319'),
(178, 11, 'Kabupaten', 'Kab. Kediri', '64184'),
(179, 11, 'Kota', 'Kediri', '64125'),
(180, 24, 'Kabupaten', 'Kab. Keerom', '99461'),
(181, 10, 'Kabupaten', 'Kab. Kendal', '51314'),
(182, 30, 'Kota', 'Kendari', '93126'),
(183, 4, 'Kabupaten', 'Kab. Kepahiang', '39319'),
(184, 17, 'Kabupaten', 'Kab. Kepulauan Anambas', '29991'),
(185, 19, 'Kabupaten', 'Kab. Kepulauan Aru', '97681'),
(186, 32, 'Kabupaten', 'Kab. Kepulauan Mentawai', '25771'),
(187, 26, 'Kabupaten', 'Kab. Kepulauan Meranti', '28791'),
(188, 31, 'Kabupaten', 'Kab. Kepulauan Sangihe', '95819'),
(189, 6, 'Kabupaten', 'Kab. Kepulauan Seribu', '14550'),
(190, 31, 'Kabupaten', 'Kab. Kepulauan Siau Tagulandang Biaro (Sitaro)', '95862'),
(191, 20, 'Kabupaten', 'Kab. Kepulauan Sula', '97995'),
(192, 31, 'Kabupaten', 'Kab. Kepulauan Talaud', '95885'),
(193, 24, 'Kabupaten', 'Kab. Kepulauan Yapen (Yapen Waropen)', '98211'),
(194, 8, 'Kabupaten', 'Kab. Kerinci', '37167'),
(195, 12, 'Kabupaten', 'Kab. Ketapang', '78874'),
(196, 10, 'Kabupaten', 'Kab. Klaten', '57411'),
(197, 1, 'Kabupaten', 'Kab. Klungkung', '80719'),
(198, 30, 'Kabupaten', 'Kab. Kolaka', '93511'),
(199, 30, 'Kabupaten', 'Kab. Kolaka Utara', '93911'),
(200, 30, 'Kabupaten', 'Kab. Konawe', '93411'),
(201, 30, 'Kabupaten', 'Kab. Konawe Selatan', '93811'),
(202, 30, 'Kabupaten', 'Kab. Konawe Utara', '93311'),
(203, 13, 'Kabupaten', 'Kab. Kotabaru', '72119'),
(204, 31, 'Kota', 'Kotamobagu', '95711'),
(205, 14, 'Kabupaten', 'Kab. Kotawaringin Barat', '74119'),
(206, 14, 'Kabupaten', 'Kab. Kotawaringin Timur', '74364'),
(207, 26, 'Kabupaten', 'Kab. Kuantan Singingi', '29519'),
(208, 12, 'Kabupaten', 'Kab. Kubu Raya', '78311'),
(209, 10, 'Kabupaten', 'Kab. Kudus', '59311'),
(210, 5, 'Kabupaten', 'Kab. Kulon Progo', '55611'),
(211, 9, 'Kabupaten', 'Kab. Kuningan', '45511'),
(212, 23, 'Kabupaten', 'Kab. Kupang', '85362'),
(213, 23, 'Kota', 'Kupang', '85119'),
(214, 15, 'Kabupaten', 'Kab. Kutai Barat', '75711'),
(215, 15, 'Kabupaten', 'Kab. Kutai Kartanegara', '75511'),
(216, 15, 'Kabupaten', 'Kab. Kutai Timur', '75611'),
(217, 34, 'Kabupaten', 'Kab. Labuhan Batu', '21412'),
(218, 34, 'Kabupaten', 'Kab. Labuhan Batu Selatan', '21511'),
(219, 34, 'Kabupaten', 'Kab. Labuhan Batu Utara', '21711'),
(220, 33, 'Kabupaten', 'Kab. Lahat', '31419'),
(221, 14, 'Kabupaten', 'Kab. Lamandau', '74611'),
(222, 11, 'Kabupaten', 'Kab. Lamongan', '64125'),
(223, 18, 'Kabupaten', 'Kab. Lampung Barat', '34814'),
(224, 18, 'Kabupaten', 'Kab. Lampung Selatan', '35511'),
(225, 18, 'Kabupaten', 'Kab. Lampung Tengah', '34212'),
(226, 18, 'Kabupaten', 'Kab. Lampung Timur', '34319'),
(227, 18, 'Kabupaten', 'Kab. Lampung Utara', '34516'),
(228, 12, 'Kabupaten', 'Kab. Landak', '78319'),
(229, 34, 'Kabupaten', 'Kab. Langkat', '20811'),
(230, 21, 'Kota', 'Langsa', '24412'),
(231, 24, 'Kabupaten', 'Kab. Lanny Jaya', '99531'),
(232, 3, 'Kabupaten', 'Kab. Lebak', '42319'),
(233, 4, 'Kabupaten', 'Kab. Lebong', '39264'),
(234, 23, 'Kabupaten', 'Kab. Lembata', '86611'),
(235, 21, 'Kota', 'Lhokseumawe', '24352'),
(236, 32, 'Kabupaten', 'Kab. Lima Puluh Koto/Kota', '26671'),
(237, 17, 'Kabupaten', 'Kab. Lingga', '29811'),
(238, 22, 'Kabupaten', 'Kab. Lombok Barat', '83311'),
(239, 22, 'Kabupaten', 'Kab. Lombok Tengah', '83511'),
(240, 22, 'Kabupaten', 'Kab. Lombok Timur', '83612'),
(241, 22, 'Kabupaten', 'Kab. Lombok Utara', '83711'),
(242, 33, 'Kota', 'Lubuk Linggau', '31614'),
(243, 11, 'Kabupaten', 'Kab. Lumajang', '67319'),
(244, 28, 'Kabupaten', 'Kab. Luwu', '91994'),
(245, 28, 'Kabupaten', 'Kab. Luwu Timur', '92981'),
(246, 28, 'Kabupaten', 'Kab. Luwu Utara', '92911'),
(247, 11, 'Kabupaten', 'Kab. Madiun', '63153'),
(248, 11, 'Kota', 'Madiun', '63122'),
(249, 10, 'Kabupaten', 'Kab. Magelang', '56519'),
(250, 10, 'Kota', 'Magelang', '56133'),
(251, 11, 'Kabupaten', 'Kab. Magetan', '63314'),
(252, 9, 'Kabupaten', 'Kab. Majalengka', '45412'),
(253, 27, 'Kabupaten', 'Kab. Majene', '91411'),
(254, 28, 'Kota', 'Makassar', '90111'),
(255, 11, 'Kabupaten', 'Kab. Malang', '65163'),
(256, 11, 'Kota', 'Malang', '65112'),
(257, 16, 'Kabupaten', 'Kab. Malinau', '77511'),
(258, 19, 'Kabupaten', 'Kab. Maluku Barat Daya', '97451'),
(259, 19, 'Kabupaten', 'Kab. Maluku Tengah', '97513'),
(260, 19, 'Kabupaten', 'Kab. Maluku Tenggara', '97651'),
(261, 19, 'Kabupaten', 'Kab. Maluku Tenggara Barat', '97465'),
(262, 27, 'Kabupaten', 'Kab. Mamasa', '91362'),
(263, 24, 'Kabupaten', 'Kab. Mamberamo Raya', '99381'),
(264, 24, 'Kabupaten', 'Kab. Mamberamo Tengah', '99553'),
(265, 27, 'Kabupaten', 'Kab. Mamuju', '91519'),
(266, 27, 'Kabupaten', 'Kab. Mamuju Utara', '91571'),
(267, 31, 'Kota', 'Manado', '95247'),
(268, 34, 'Kabupaten', 'Kab. Mandailing Natal', '22916'),
(269, 23, 'Kabupaten', 'Kab. Manggarai', '86551'),
(270, 23, 'Kabupaten', 'Kab. Manggarai Barat', '86711'),
(271, 23, 'Kabupaten', 'Kab. Manggarai Timur', '86811'),
(272, 25, 'Kabupaten', 'Kab. Manokwari', '98311'),
(273, 25, 'Kabupaten', 'Kab. Manokwari Selatan', '98355'),
(274, 24, 'Kabupaten', 'Kab. Mappi', '99853'),
(275, 28, 'Kabupaten', 'Kab. Maros', '90511'),
(276, 22, 'Kota', 'Mataram', '83131'),
(277, 25, 'Kabupaten', 'Kab. Maybrat', '98051'),
(278, 34, 'Kota', 'Medan', '20228'),
(279, 12, 'Kabupaten', 'Kab. Melawi', '78619'),
(280, 8, 'Kabupaten', 'Kab. Merangin', '37319'),
(281, 24, 'Kabupaten', 'Kab. Merauke', '99613'),
(282, 18, 'Kabupaten', 'Kab. Mesuji', '34911'),
(283, 18, 'Kota', 'Metro', '34111'),
(284, 24, 'Kabupaten', 'Kab. Mimika', '99962'),
(285, 31, 'Kabupaten', 'Kab. Minahasa', '95614'),
(286, 31, 'Kabupaten', 'Kab. Minahasa Selatan', '95914'),
(287, 31, 'Kabupaten', 'Kab. Minahasa Tenggara', '95995'),
(288, 31, 'Kabupaten', 'Kab. Minahasa Utara', '95316'),
(289, 11, 'Kabupaten', 'Kab. Mojokerto', '61382'),
(290, 11, 'Kota', 'Mojokerto', '61316'),
(291, 29, 'Kabupaten', 'Kab. Morowali', '94911'),
(292, 33, 'Kabupaten', 'Kab. Muara Enim', '31315'),
(293, 8, 'Kabupaten', 'Kab. Muaro Jambi', '36311'),
(294, 4, 'Kabupaten', 'Kab. Muko Muko', '38715'),
(295, 30, 'Kabupaten', 'Kab. Muna', '93611'),
(296, 14, 'Kabupaten', 'Kab. Murung Raya', '73911'),
(297, 33, 'Kabupaten', 'Kab. Musi Banyuasin', '30719'),
(298, 33, 'Kabupaten', 'Kab. Musi Rawas', '31661'),
(299, 24, 'Kabupaten', 'Kab. Nabire', '98816'),
(300, 21, 'Kabupaten', 'Kab. Nagan Raya', '23674'),
(301, 23, 'Kabupaten', 'Kab. Nagekeo', '86911'),
(302, 17, 'Kabupaten', 'Kab. Natuna', '29711'),
(303, 24, 'Kabupaten', 'Kab. Nduga', '99541'),
(304, 23, 'Kabupaten', 'Kab. Ngada', '86413'),
(305, 11, 'Kabupaten', 'Kab. Nganjuk', '64414'),
(306, 11, 'Kabupaten', 'Kab. Ngawi', '63219'),
(307, 34, 'Kabupaten', 'Kab. Nias', '22876'),
(308, 34, 'Kabupaten', 'Kab. Nias Barat', '22895'),
(309, 34, 'Kabupaten', 'Kab. Nias Selatan', '22865'),
(310, 34, 'Kabupaten', 'Kab. Nias Utara', '22856'),
(311, 16, 'Kabupaten', 'Kab. Nunukan', '77421'),
(312, 33, 'Kabupaten', 'Kab. Ogan Ilir', '30811'),
(313, 33, 'Kabupaten', 'Kab. Ogan Komering Ilir', '30618'),
(314, 33, 'Kabupaten', 'Kab. Ogan Komering Ulu', '32112'),
(315, 33, 'Kabupaten', 'Kab. Ogan Komering Ulu Selatan', '32211'),
(316, 33, 'Kabupaten', 'Kab. Ogan Komering Ulu Timur', '32312'),
(317, 11, 'Kabupaten', 'Kab. Pacitan', '63512'),
(318, 32, 'Kota', 'Padang', '25112'),
(319, 34, 'Kabupaten', 'Kab. Padang Lawas', '22763'),
(320, 34, 'Kabupaten', 'Kab. Padang Lawas Utara', '22753'),
(321, 32, 'Kota', 'Padang Panjang', '27122'),
(322, 32, 'Kabupaten', 'Kab. Padang Pariaman', '25583'),
(323, 34, 'Kota', 'Padang Sidempuan', '22727'),
(324, 33, 'Kota', 'Pagar Alam', '31512'),
(325, 34, 'Kabupaten', 'Kab. Pakpak Bharat', '22272'),
(326, 14, 'Kota', 'Palangka Raya', '73112'),
(327, 33, 'Kota', 'Palembang', '31512'),
(328, 28, 'Kota', 'Palopo', '91911'),
(329, 29, 'Kota', 'Palu', '94111'),
(330, 11, 'Kabupaten', 'Kab. Pamekasan', '69319'),
(331, 3, 'Kabupaten', 'Kab. Pandeglang', '42212'),
(332, 9, 'Kabupaten', 'Kab. Pangandaran', '46511'),
(333, 28, 'Kabupaten', 'Kab. Pangkajene Kepulauan', '90611'),
(334, 2, 'Kota', 'Pangkal Pinang', '33115'),
(335, 24, 'Kabupaten', 'Kab. Paniai', '98765'),
(336, 28, 'Kota', 'Parepare', '91123'),
(337, 32, 'Kota', 'Pariaman', '25511'),
(338, 29, 'Kabupaten', 'Kab. Parigi Moutong', '94411'),
(339, 32, 'Kabupaten', 'Kab. Pasaman', '26318'),
(340, 32, 'Kabupaten', 'Kab. Pasaman Barat', '26511'),
(341, 15, 'Kabupaten', 'Kab. Paser', '76211'),
(342, 11, 'Kabupaten', 'Kab. Pasuruan', '67153'),
(343, 11, 'Kota', 'Pasuruan', '67118'),
(344, 10, 'Kabupaten', 'Kab. Pati', '59114'),
(345, 32, 'Kota', 'Payakumbuh', '26213'),
(346, 25, 'Kabupaten', 'Kab. Pegunungan Arfak', '98354'),
(347, 24, 'Kabupaten', 'Kab. Pegunungan Bintang', '99573'),
(348, 10, 'Kabupaten', 'Kab. Pekalongan', '51161'),
(349, 10, 'Kota', 'Pekalongan', '51122'),
(350, 26, 'Kota', 'Pekanbaru', '28112'),
(351, 26, 'Kabupaten', 'Kab. Pelalawan', '28311'),
(352, 10, 'Kabupaten', 'Kab. Pemalang', '52319'),
(353, 34, 'Kota', 'Pematang Siantar', '21126'),
(354, 15, 'Kabupaten', 'Kab. Penajam Paser Utara', '76311'),
(355, 18, 'Kabupaten', 'Kab. Pesawaran', '35312'),
(356, 18, 'Kabupaten', 'Kab. Pesisir Barat', '35974'),
(357, 32, 'Kabupaten', 'Kab. Pesisir Selatan', '25611'),
(358, 21, 'Kabupaten', 'Kab. Pidie', '24116'),
(359, 21, 'Kabupaten', 'Kab. Pidie Jaya', '24186'),
(360, 28, 'Kabupaten', 'Kab. Pinrang', '91251'),
(361, 7, 'Kabupaten', 'Kab. Pohuwato', '96419'),
(362, 27, 'Kabupaten', 'Kab. Polewali Mandar', '91311'),
(363, 11, 'Kabupaten', 'Kab. Ponorogo', '63411'),
(364, 12, 'Kabupaten', 'Kab. Pontianak', '78971'),
(365, 12, 'Kota', 'Pontianak', '78112'),
(366, 29, 'Kabupaten', 'Kab. Poso', '94615'),
(367, 33, 'Kota', 'Prabumulih', '31121'),
(368, 18, 'Kabupaten', 'Kab. Pringsewu', '35719'),
(369, 11, 'Kabupaten', 'Kab. Probolinggo', '67282'),
(370, 11, 'Kota', 'Probolinggo', '67215'),
(371, 14, 'Kabupaten', 'Kab. Pulang Pisau', '74811'),
(372, 20, 'Kabupaten', 'Kab. Pulau Morotai', '97771'),
(373, 24, 'Kabupaten', 'Kab. Puncak', '98981'),
(374, 24, 'Kabupaten', 'Kab. Puncak Jaya', '98979'),
(375, 10, 'Kabupaten', 'Kab. Purbalingga', '53312'),
(376, 9, 'Kabupaten', 'Kab. Purwakarta', '41119'),
(377, 10, 'Kabupaten', 'Kab. Purworejo', '54111'),
(378, 25, 'Kabupaten', 'Kab. Raja Ampat', '98489'),
(379, 4, 'Kabupaten', 'Kab. Rejang Lebong', '39112'),
(380, 10, 'Kabupaten', 'Kab. Rembang', '59219'),
(381, 26, 'Kabupaten', 'Kab. Rokan Hilir', '28992'),
(382, 26, 'Kabupaten', 'Kab. Rokan Hulu', '28511'),
(383, 23, 'Kabupaten', 'Kab. Rote Ndao', '85982'),
(384, 21, 'Kota', 'Sabang', '23512'),
(385, 23, 'Kabupaten', 'Kab. Sabu Raijua', '85391'),
(386, 10, 'Kota', 'Salatiga', '50711'),
(387, 15, 'Kota', 'Samarinda', '75133'),
(388, 12, 'Kabupaten', 'Kab. Sambas', '79453'),
(389, 34, 'Kabupaten', 'Kab. Samosir', '22392'),
(390, 11, 'Kabupaten', 'Kab. Sampang', '69219'),
(391, 12, 'Kabupaten', 'Kab. Sanggau', '78557'),
(392, 24, 'Kabupaten', 'Kab. Sarmi', '99373'),
(393, 8, 'Kabupaten', 'Kab. Sarolangun', '37419'),
(394, 32, 'Kota', 'Sawah Lunto', '27416'),
(395, 12, 'Kabupaten', 'Kab. Sekadau', '79583'),
(396, 28, 'Kabupaten', 'Kab. Selayar (Kepulauan Selayar)', '92812'),
(397, 4, 'Kabupaten', 'Kab. Seluma', '38811'),
(398, 10, 'Kabupaten', 'Kab. Semarang', '50511'),
(399, 10, 'Kota', 'Semarang', '50135'),
(400, 19, 'Kabupaten', 'Kab. Seram Bagian Barat', '97561'),
(401, 19, 'Kabupaten', 'Kab. Seram Bagian Timur', '97581'),
(402, 3, 'Kabupaten', 'Kab. Serang', '42182'),
(403, 3, 'Kota', 'Serang', '42111'),
(404, 34, 'Kabupaten', 'Kab. Serdang Bedagai', '20915'),
(405, 14, 'Kabupaten', 'Kab. Seruyan', '74211'),
(406, 26, 'Kabupaten', 'Kab. Siak', '28623'),
(407, 34, 'Kota', 'Sibolga', '22522'),
(408, 28, 'Kabupaten', 'Kab. Sidenreng Rappang/Rapang', '91613'),
(409, 11, 'Kabupaten', 'Kab. Sidoarjo', '61219'),
(410, 29, 'Kabupaten', 'Kab. Sigi', '94364'),
(411, 32, 'Kabupaten', 'Kab. Sijunjung (Sawah Lunto Sijunjung)', '27511'),
(412, 23, 'Kabupaten', 'Kab. Sikka', '86121'),
(413, 34, 'Kabupaten', 'Kab. Simalungun', '21162'),
(414, 21, 'Kabupaten', 'Kab. Simeulue', '23891'),
(415, 12, 'Kota', 'Singkawang', '79117'),
(416, 28, 'Kabupaten', 'Kab. Sinjai', '92615'),
(417, 12, 'Kabupaten', 'Kab. Sintang', '78619'),
(418, 11, 'Kabupaten', 'Kab. Situbondo', '68316'),
(419, 5, 'Kabupaten', 'Kab. Sleman', '55513'),
(420, 32, 'Kabupaten', 'Kab. Solok', '27365'),
(421, 32, 'Kota', 'Solok', '27315'),
(422, 32, 'Kabupaten', 'Kab. Solok Selatan', '27779'),
(423, 28, 'Kabupaten', 'Kab. Soppeng', '90812'),
(424, 25, 'Kabupaten', 'Kab. Sorong', '98431'),
(425, 25, 'Kota', 'Sorong', '98411'),
(426, 25, 'Kabupaten', 'Kab. Sorong Selatan', '98454'),
(427, 10, 'Kabupaten', 'Kab. Sragen', '57211'),
(428, 9, 'Kabupaten', 'Kab. Subang', '41215'),
(429, 21, 'Kota', 'Subulussalam', '24882'),
(430, 9, 'Kabupaten', 'Kab. Sukabumi', '43311'),
(431, 9, 'Kota', 'Sukabumi', '43114'),
(432, 14, 'Kabupaten', 'Kab. Sukamara', '74712'),
(433, 10, 'Kabupaten', 'Kab. Sukoharjo', '57514'),
(434, 23, 'Kabupaten', 'Kab. Sumba Barat', '87219'),
(435, 23, 'Kabupaten', 'Kab. Sumba Barat Daya', '87453'),
(436, 23, 'Kabupaten', 'Kab. Sumba Tengah', '87358'),
(437, 23, 'Kabupaten', 'Kab. Sumba Timur', '87112'),
(438, 22, 'Kabupaten', 'Kab. Sumbawa', '84315'),
(439, 22, 'Kabupaten', 'Kab. Sumbawa Barat', '84419'),
(440, 9, 'Kabupaten', 'Kab. Sumedang', '45326'),
(441, 11, 'Kabupaten', 'Kab. Sumenep', '69413'),
(442, 8, 'Kota', 'Sungaipenuh', '37113'),
(443, 24, 'Kabupaten', 'Kab. Supiori', '98164'),
(444, 11, 'Kota', 'Surabaya', '60119'),
(445, 10, 'Kota', 'Surakarta (Solo)', '57113'),
(446, 13, 'Kabupaten', 'Kab. Tabalong', '71513'),
(447, 1, 'Kabupaten', 'Kab. Tabanan', '82119'),
(448, 28, 'Kabupaten', 'Kab. Takalar', '92212'),
(449, 25, 'Kabupaten', 'Kab. Tambrauw', '98475'),
(450, 16, 'Kabupaten', 'Kab. Tana Tidung', '77611'),
(451, 28, 'Kabupaten', 'Kab. Tana Toraja', '91819'),
(452, 13, 'Kabupaten', 'Kab. Tanah Bumbu', '72211'),
(453, 32, 'Kabupaten', 'Kab. Tanah Datar', '27211'),
(454, 13, 'Kabupaten', 'Kab. Tanah Laut', '70811'),
(455, 3, 'Kabupaten', 'Kab. Tangerang', '15914'),
(456, 3, 'Kota', 'Tangerang', '15111'),
(457, 3, 'Kota', 'Tangerang Selatan', '15332'),
(458, 18, 'Kabupaten', 'Kab. Tanggamus', '35619'),
(459, 34, 'Kota', 'Tanjung Balai', '21321'),
(460, 8, 'Kabupaten', 'Kab. Tanjung Jabung Barat', '36513'),
(461, 8, 'Kabupaten', 'Kab. Tanjung Jabung Timur', '36719'),
(462, 17, 'Kota', 'Tanjung Pinang', '29111'),
(463, 34, 'Kabupaten', 'Kab. Tapanuli Selatan', '22742'),
(464, 34, 'Kabupaten', 'Kab. Tapanuli Tengah', '22611'),
(465, 34, 'Kabupaten', 'Kab. Tapanuli Utara', '22414'),
(466, 13, 'Kabupaten', 'Kab. Tapin', '71119'),
(467, 16, 'Kota', 'Tarakan', '77114'),
(468, 9, 'Kabupaten', 'Kab. Tasikmalaya', '46411'),
(469, 9, 'Kota', 'Tasikmalaya', '46116'),
(470, 34, 'Kota', 'Tebing Tinggi', '20632'),
(471, 8, 'Kabupaten', 'Kab. Tebo', '37519'),
(472, 10, 'Kabupaten', 'Kab. Tegal', '52419'),
(473, 10, 'Kota', 'Tegal', '52114'),
(474, 25, 'Kabupaten', 'Kab. Teluk Bintuni', '98551'),
(475, 25, 'Kabupaten', 'Kab. Teluk Wondama', '98591'),
(476, 10, 'Kabupaten', 'Kab. Temanggung', '56212'),
(477, 20, 'Kota', 'Ternate', '97714'),
(478, 20, 'Kota', 'Tidore Kepulauan', '97815'),
(479, 23, 'Kabupaten', 'Kab. Timor Tengah Selatan', '85562'),
(480, 23, 'Kabupaten', 'Kab. Timor Tengah Utara', '85612'),
(481, 34, 'Kabupaten', 'Kab. Toba Samosir', '22316'),
(482, 29, 'Kabupaten', 'Kab. Tojo Una-Una', '94683'),
(483, 29, 'Kabupaten', 'Kab. Toli-Toli', '94542'),
(484, 24, 'Kabupaten', 'Kab. Tolikara', '99411'),
(485, 31, 'Kota', 'Tomohon', '95416'),
(486, 28, 'Kabupaten', 'Kab. Toraja Utara', '91831'),
(487, 11, 'Kabupaten', 'Kab. Trenggalek', '66312'),
(488, 19, 'Kota', 'Tual', '97612'),
(489, 11, 'Kabupaten', 'Kab. Tuban', '62319'),
(490, 18, 'Kabupaten', 'Kab. Tulang Bawang', '34613'),
(491, 18, 'Kabupaten', 'Kab. Tulang Bawang Barat', '34419'),
(492, 11, 'Kabupaten', 'Kab. Tulungagung', '66212'),
(493, 28, 'Kabupaten', 'Kab. Wajo', '90911'),
(494, 30, 'Kabupaten', 'Kab. Wakatobi', '93791'),
(495, 24, 'Kabupaten', 'Kab. Waropen', '98269'),
(496, 18, 'Kabupaten', 'Kab. Way Kanan', '34711'),
(497, 10, 'Kabupaten', 'Kab. Wonogiri', '57619'),
(498, 10, 'Kabupaten', 'Kab. Wonosobo', '56311'),
(499, 24, 'Kabupaten', 'Kab. Yahukimo', '99041'),
(500, 24, 'Kabupaten', 'Kab. Yalimo', '99481'),
(501, 5, 'Kota', 'Yogyakarta', '55222');

-- --------------------------------------------------------

--
-- Table structure for table `t_order`
--

CREATE TABLE `t_order` (
  `id_order` int(11) NOT NULL,
  `kode_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_order` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `kurir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_paket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lama_pengiriman` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_pengiriman` int(11) NOT NULL,
  `status_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_order`
--

INSERT INTO `t_order` (`id_order`, `kode_order`, `tanggal_order`, `email`, `alamat`, `id_provinsi`, `id_kota`, `postal_code`, `kurir`, `jenis_paket`, `lama_pengiriman`, `harga_pengiriman`, `status_order`) VALUES
(1, '20191228C27MQXUEQVKLJ9D8', 1577546289, 'sandhikagalih@unpas.ac.id', 'kampus unpas', 9, 23, 40111, 'Jalur Nugraha Ekakurir (JNE)', 'CTC', '1-2', 8000, '0');

-- --------------------------------------------------------

--
-- Table structure for table `t_pembayaran`
--

CREATE TABLE `t_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `kode_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pembayaran` int(11) NOT NULL,
  `nama_nasabah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bank` int(11) NOT NULL,
  `jumlah_transfer` bigint(20) NOT NULL,
  `keterangan_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_bukti` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_produk`
--

CREATE TABLE `t_produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jenis_produk` int(11) NOT NULL,
  `deskripsi_produk` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `diskon` int(11) DEFAULT 0,
  `stok_produk` int(11) NOT NULL,
  `gambar_produk` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_produk`
--

INSERT INTO `t_produk` (`id_produk`, `nama_produk`, `id_jenis_produk`, `deskripsi_produk`, `harga_produk`, `diskon`, `stok_produk`, `gambar_produk`) VALUES
(8, 'sepatu olahraga pria', 3, 'sepatu olah raga', 120000, 10, 39, 'product-31.jpg'),
(9, 'baju wanita abu-abu', 2, 'baju level legendary', 150000, 15, 35, 'item-7.jpg'),
(10, 'baju putih', 1, 'warnanya cerah sekali', 200000, 5, 35, 'item-62.jpg'),
(12, 'baju batik', 1, 'baju asli indonesia', 110000, 0, 50, 'batik1.jpg'),
(13, 'jaket mentari', 2, 'jaket yang memiliki rasa ceria', 175000, 0, 30, 'item-14.jpg'),
(14, 't-shirt imut', 2, 'pakaian imut asli buatan tangan pengrajin', 120000, 0, 90, 'model_3_bg.jpg'),
(15, 'jas bengawan', 1, 'jas pria buatan wong solo', 105000, 0, 30, 'model_2_bg.jpg'),
(16, 'kemeja rimas', 2, 'kemeja wanita terbaik', 100000, 0, 40, 'kemeja1.jpg'),
(17, 'sepatu kulit cokelat', 3, 'sepatu buatan katapang', 90000, 0, 20, 'product_1_bg.jpg'),
(18, 'baju owalah', 2, 'owalah cantik', 95000, 0, 25, 'model_4_bg.jpg'),
(19, 'Jaket Kulit', 1, 'Jaket kulit buatan kabupaten Garut', 85000, 0, 15, 'bg_1.jpg'),
(20, 'sweater rusa', 2, 'sweater', 115000, 0, 20, 'prodect_details_4.png'),
(21, 'Sepatu olahraga wanita', 3, 'sepatu olahraga khusus wanita', 80000, 0, 25, 'product-8.png'),
(23, 'kemeja kotak biru', 1, 'kemeja kotak bagus', 135000, 0, 35, 'prodect_details_2.png'),
(24, 'Jaket hitam', 1, 'jaket hitam', 125000, 0, 20, 'prodect_details_3.png'),
(25, 'Baju polo wanita', 2, 'baju polo', 75000, 0, 50, 'bajupolowanita1.jpg'),
(27, 'sepatu kulit', 3, 'sepatu kulit', 125000, 0, 50, 'product-6.png'),
(28, 'baju olahraga wanita', 2, 'baju olah raga', 70000, 0, 45, 'bajuorwanita1.jpg'),
(29, 'baju olahraga pria', 1, 'baju olahraga', 80000, 0, 60, 'bajuorpria1.jpg'),
(30, 'baju polo pria', 1, 'baju polo', 97000, 0, 45, 'bajupolopria1.jpg'),
(31, 'jas hitam', 1, 'jas hitam', 120000, 0, 45, 'item-9.jpg'),
(32, 'jaket putih', 2, 'jaket', 115000, 20, 60, 'category_5.png'),
(33, 'sweater merah jambu', 2, 'sweater', 120000, 0, 50, 'item-5.jpg'),
(34, 'celana jeans denim', 2, 'celana', 80000, 0, 50, 's-product-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `t_provinsi`
--

CREATE TABLE `t_provinsi` (
  `id_provinsi` bigint(20) NOT NULL,
  `nama_provinsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_provinsi`
--

INSERT INTO `t_provinsi` (`id_provinsi`, `nama_provinsi`) VALUES
(1, 'Bali'),
(2, 'Bangka Belitung'),
(3, 'Banten'),
(4, 'Bengkulu'),
(5, 'DI Yogyakarta'),
(6, 'DKI Jakarta'),
(7, 'Gorontalo'),
(8, 'Jambi'),
(9, 'Jawa Barat'),
(10, 'Jawa Tengah'),
(11, 'Jawa Timur'),
(12, 'Kalimantan Barat'),
(13, 'Kalimantan Selatan'),
(14, 'Kalimantan Tengah'),
(15, 'Kalimantan Timur'),
(16, 'Kalimantan Utara'),
(17, 'Kepulauan Riau'),
(18, 'Lampung'),
(19, 'Maluku'),
(20, 'Maluku Utara'),
(21, 'Nanggroe Aceh Darussalam'),
(22, 'Nusa Tenggara Barat'),
(23, 'Nusa Tenggara Timur'),
(24, 'Papua'),
(25, 'Papua Barat'),
(26, 'Riau'),
(27, 'Sulawesi Barat'),
(28, 'Sulawesi Selatan'),
(29, 'Sulawesi Tengah'),
(30, 'Sulawesi Tenggara'),
(31, 'Sulawesi Utara'),
(32, 'Sumatera Barat'),
(33, 'Sumatera Selatan'),
(34, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` int(11) NOT NULL,
  `id_jenis_kelamin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `cookie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `username`, `tanggal_lahir`, `id_jenis_kelamin`, `email`, `no_telepon`, `image`, `password`, `is_active`, `date_created`, `cookie`) VALUES
(3, 'Fauzan Rizkyana G', 1017792000, 1, 'fauzanrizkyana@gmail.com', '0212345678', 'default.jpg', '$2y$10$tEE5RlytwJYMsYy0mU3iQOKmBzJPYDKOAdr/8YF4l/twwk/bnH4i.', 1, 1573454283, 'hzT0lxU2PakHmflvF995Wr8T2ocVw7GH6Z0DpqsfhRB3j4x8u7sA1cadjOZtRuyg'),
(4, 'Sandhika Galih', 788893200, 1, 'sandhikagalih@unpas.ac.id', '081876543210', 'sandhikagalih1.JPG', '$2y$10$MYHkuz0AV7FrpvRpK2EdhOH/63QlrOatNBADWojFDZxUICSA91uRm', 1, 1573455306, 'Xo0vHr63Bm9bcqmRknY4H1hMsPjeC9ZEeKVda7CJNTxQO0fuSaBII8XGYxSR5ywg'),
(5, 'Doddy Ferdiansyah', 883949493, 1, 'doddy@gmail.com', '0223456789', 'default.jpg', '$2y$10$zNw8QljSqwKMt64Ns9UDyODpeoB.TqxRPd2bfjuJWLo1devc8QAjK', 1, 1573459293, NULL),
(6, 'Nofariza Handayani', 999212222, 2, 'nofa@yahoo.com', '082123456789', 'default.jpg', '$2y$10$1KMHjXwggoocgU64XxyZwuDnUY.mdFl0UjEz.jm40clFKk3tyHGz2', 1, 1576225810, NULL),
(7, 'Erik', 819392400, 1, 'erik@gmail.com', '081234567890', 'default.jpg', '$2y$10$M7HxOMo.IphVKCFU.0NiA.0rh5gmlkvea7.h1j1bMuj8KOf.VYh3G', 1, 1576659957, NULL),
(8, 'Meli', 946746000, 2, 'meli@gmail.com', '0222222222', 'default.jpg', '$2y$10$AJq3U3/4dTOhJvj0/qcMbOgTXfFHMQ.s66X7BhWxBXt7Y9eNENIyq', 1, 1576661150, NULL),
(9, 'Viral Dong', 1045242000, 1, 'viraldong@gmail.com', '081123456780', 'IMG_20160527_151615.jpg', '$2y$10$XaJnEs/VMyMZ1bkB4QmBE.JJ3.g7.sQcDLK.R9mnM8moSZwpxcLlO', 1, 1576721919, '5YuGCPHyxuFOW7rkmfd8c6wlM4enYUjT3ZRjdM0XyJqsqWDl9FSspER9v3w2tkLB'),
(10, 'Mercy', 874256400, 2, 'mercy@gmail.com', '082112233445', 'default.jpg', '$2y$10$Uqdy4lQtopTGvpj1et.I/.FoXKmuAKnn14W6ONIM4dMMO8GNK5vDi', 1, 1576744187, 'Mbho7wtgD6IcdirBMAaeRE8XhURlfOvCbG3Lt1IAg20WNoJWmBF4ZDYm9ey1fnLa');

-- --------------------------------------------------------

--
-- Table structure for table `t_wishlist`
--

CREATE TABLE `t_wishlist` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_wishlist`
--

INSERT INTO `t_wishlist` (`id`, `email`, `id_produk`) VALUES
(1, 'sandhikagalih@unpas.ac.id', 14),
(3, 'doddy@gmail.com', 29),
(7, 'sandhikagalih@unpas.ac.id', 6),
(8, 'fauzanrizkyana@gmail.com', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_about`
--
ALTER TABLE `t_about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `t_bank`
--
ALTER TABLE `t_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `t_banner`
--
ALTER TABLE `t_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_barang_order`
--
ALTER TABLE `t_barang_order`
  ADD PRIMARY KEY (`id_barang_order`);

--
-- Indexes for table `t_berita`
--
ALTER TABLE `t_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `t_feedback`
--
ALTER TABLE `t_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_kelamin`
--
ALTER TABLE `t_jenis_kelamin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kota`
--
ALTER TABLE `t_kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `t_order`
--
ALTER TABLE `t_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `t_pembayaran`
--
ALTER TABLE `t_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `t_produk`
--
ALTER TABLE `t_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `t_provinsi`
--
ALTER TABLE `t_provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `t_wishlist`
--
ALTER TABLE `t_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_about`
--
ALTER TABLE `t_about`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `t_bank`
--
ALTER TABLE `t_bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `t_banner`
--
ALTER TABLE `t_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `t_barang_order`
--
ALTER TABLE `t_barang_order`
  MODIFY `id_barang_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_berita`
--
ALTER TABLE `t_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_feedback`
--
ALTER TABLE `t_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `t_jenis_kelamin`
--
ALTER TABLE `t_jenis_kelamin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_kota`
--
ALTER TABLE `t_kota`
  MODIFY `id_kota` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `t_order`
--
ALTER TABLE `t_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_pembayaran`
--
ALTER TABLE `t_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_produk`
--
ALTER TABLE `t_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `t_provinsi`
--
ALTER TABLE `t_provinsi`
  MODIFY `id_provinsi` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t_wishlist`
--
ALTER TABLE `t_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
